excel.mathTrig = {};
excel.mathTrig.ABS = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.abs(excel.utils.parseNumber(number));
};

excel.mathTrig.ACOS = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.acos(number);
};

excel.mathTrig.ACOSH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.log(number + Math.sqrt(number * number - 1));
};

excel.mathTrig.ACOT = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.atan(1 / number);
};

excel.mathTrig.ACOTH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return 0.5 * Math.log((number + 1) / (number - 1));
};

// TODO: use options
excel.mathTrig.AGGREGATE = function(function_num, options, ref1, ref2) {
	function_num = excel.utils.parseNumber(function_num);
	options = excel.utils.parseNumber(function_num);
	if (excel.utils.anyIsError(function_num, options)) {
		return excel.error.value;
	}
	switch (function_num) {
	case 1:
		return excel.statistical.AVERAGE(ref1);
	case 2:
		return excel.statistical.COUNT(ref1);
	case 3:
		return excel.statistical.COUNTA(ref1);
	case 4:
		return excel.statistical.MAX(ref1);
	case 5:
		return excel.statistical.MIN(ref1);
	case 6:
		return excel.mathTrig.PRODUCT(ref1);
	case 7:
		return excel.statistical.STDEV.S(ref1);
	case 8:
		return excel.statistical.STDEV.P(ref1);
	case 9:
		return excel.mathTrig.SUM(ref1);
	case 10:
		return excel.statistical.VAR.S(ref1);
	case 11:
		return excel.statistical.VAR.P(ref1);
	case 12:
		return excel.statistical.MEDIAN(ref1);
	case 13:
		return excel.statistical.MODE.SNGL(ref1);
	case 14:
		return excel.statistical.LARGE(ref1, ref2);
	case 15:
		return excel.statistical.SMALL(ref1, ref2);
	case 16:
		return excel.statistical.PERCENTILE.INC(ref1, ref2);
	case 17:
		return excel.statistical.QUARTILE.INC(ref1, ref2);
	case 18:
		return excel.statistical.PERCENTILE.EXC(ref1, ref2);
	case 19:
		return excel.statistical.QUARTILE.EXC(ref1, ref2);
	}
};

excel.mathTrig.ARABIC = function(text) {
	// Credits: Rafa? Kukawski
	if (!/^M*(?:D?C{0,3}|C[MD])(?:L?X{0,3}|X[CL])(?:V?I{0,3}|I[XV])$/
			.test(text)) {
		return excel.error.value;
	}
	var r = 0;
	text.replace(/[MDLV]|C[MD]?|X[CL]?|I[XV]?/g, function(i) {
		r += {
			M : 1000,
			CM : 900,
			D : 500,
			CD : 400,
			C : 100,
			XC : 90,
			L : 50,
			XL : 40,
			X : 10,
			IX : 9,
			V : 5,
			IV : 4,
			I : 1
		}[i];
	});
	return r;
};

excel.mathTrig.ASIN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.asin(number);
};

excel.mathTrig.ASINH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.log(number + Math.sqrt(number * number + 1));
};

excel.mathTrig.ATAN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.atan(number);
};

excel.mathTrig.ATAN2 = function(number_x, number_y) {
	number_x = excel.utils.parseNumber(number_x);
	number_y = excel.utils.parseNumber(number_y);
	if (excel.utils.anyIsError(number_x, number_y)) {
		return excel.error.value;
	}
	return Math.atan2(number_x, number_y);
};

excel.mathTrig.ATANH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.log((1 + number) / (1 - number)) / 2;
};

excel.mathTrig.BASE = function(number, radix, min_length) {
	min_length = min_length || 0;

	number = excel.utils.parseNumber(number);
	radix = excel.utils.parseNumber(radix);
	min_length = excel.utils.parseNumber(min_length);
	if (excel.utils.anyIsError(number, radix, min_length)) {
		return excel.error.value;
	}
	min_length = (min_length === undefined) ? 0 : min_length;
	var result = number.toString(radix);
	return new Array(Math.max(min_length + 1 - result.length, 0)).join('0')
			+ result;
};

excel.mathTrig.CEILING = function(number, significance, mode) {
	significance = (significance === undefined) ? 1 : Math.abs(significance);
	mode = mode || 0;

	number = excel.utils.parseNumber(number);
	significance = excel.utils.parseNumber(significance);
	mode = excel.utils.parseNumber(mode);
	if (excel.utils.anyIsError(number, significance, mode)) {
		return excel.error.value;
	}
	if (significance === 0) {
		return 0;
	}
	var precision = -Math.floor(Math.log(significance) / Math.log(10));
	if (number >= 0) {
		return excel.mathTrig.ROUND(Math.ceil(number / significance)
				* significance, precision);
	} else {
		if (mode === 0) {
			return -excel.mathTrig.ROUND(Math.floor(Math.abs(number)
					/ significance)
					* significance, precision);
		} else {
			return -excel.mathTrig.ROUND(Math.ceil(Math.abs(number)
					/ significance)
					* significance, precision);
		}
	}
};

excel.mathTrig.CEILING.MATH = excel.mathTrig.CEILING;

excel.mathTrig.CEILING.PRECISE = excel.mathTrig.CEILING;

excel.mathTrig.COMBIN = function(number, number_chosen) {
	number = excel.utils.parseNumber(number);
	number_chosen = excel.utils.parseNumber(number_chosen);
	if (excel.utils.anyIsError(number, number_chosen)) {
		return excel.error.value;
	}
	return excel.mathTrig.FACT(number)
			/ (excel.mathTrig.FACT(number_chosen) * excel.mathTrig.FACT(number
					- number_chosen));
};

excel.mathTrig.COMBINA = function(number, number_chosen) {
	number = excel.utils.parseNumber(number);
	number_chosen = excel.utils.parseNumber(number_chosen);
	if (excel.utils.anyIsError(number, number_chosen)) {
		return excel.error.value;
	}
	return (number === 0 && number_chosen === 0) ? 1 : exports.COMBIN(number
			+ number_chosen - 1, number - 1);
};

excel.mathTrig.COS = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.cos(number);
};

excel.mathTrig.COSH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return (Math.exp(number) + Math.exp(-number)) / 2;
};

excel.mathTrig.COT = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return 1 / Math.tan(number);
};

excel.mathTrig.COTH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	var e2 = Math.exp(2 * number);
	return (e2 + 1) / (e2 - 1);
};

excel.mathTrig.CSC = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return 1 / Math.sin(number);
};

excel.mathTrig.CSCH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return 2 / (Math.exp(number) - Math.exp(-number));
};

excel.mathTrig.DECIMAL = function(number, radix) {
	if (arguments.length < 1) {
		return excel.error.value;
	}

	return parseInt(number, radix);
};

excel.mathTrig.DEGREES = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return number * 180 / Math.PI;
};

excel.mathTrig.EVEN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return excel.mathTrig.CEILING(number, -2, -1);
};

excel.mathTrig.EXP = Math.exp;

var MEMOIZED_FACT = [];
excel.mathTrig.FACT = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	var n = Math.floor(number);
	if (n === 0 || n === 1) {
		return 1;
	} else if (MEMOIZED_FACT[n] > 0) {
		return MEMOIZED_FACT[n];
	} else {
		MEMOIZED_FACT[n] = excel.mathTrig.FACT(n - 1) * n;
		return MEMOIZED_FACT[n];
	}
};

excel.mathTrig.FACTDOUBLE = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	var n = Math.floor(number);
	if (n <= 0) {
		return 1;
	} else {
		return n * excel.mathTrig.FACTDOUBLE(n - 2);
	}
};

excel.mathTrig.FLOOR = function(number, significance) {
	number = excel.utils.parseNumber(number);
	significance = excel.utils.parseNumber(significance);
	if (excel.utils.anyIsError(number, significance)) {
		return excel.error.value;
	}
	if (significance === 0) {
		return 0;
	}

	if (!(number > 0 && significance > 0) && !(number < 0 && significance < 0)) {
		return excel.error.num;
	}

	significance = Math.abs(significance);
	var precision = -Math.floor(Math.log(significance) / Math.log(10));
	if (number >= 0) {
		return excel.mathTrig.ROUND(Math.floor(number / significance)
				* significance, precision);
	} else {
		return -excel.mathTrig.ROUND(
				Math.ceil(Math.abs(number) / significance), precision);
	}
};

// TODO: Verify
excel.mathTrig.FLOOR.MATH = function(number, significance, mode) {
	significance = (significance === undefined) ? 1 : significance;
	mode = (mode === undefined) ? 0 : mode;

	number = excel.utils.parseNumber(number);
	significance = excel.utils.parseNumber(significance);
	mode = excel.utils.parseNumber(mode);
	if (excel.utils.anyIsError(number, significance, mode)) {
		return excel.error.value;
	}
	if (significance === 0) {
		return 0;
	}

	significance = significance ? Math.abs(significance) : 1;
	var precision = -Math.floor(Math.log(significance) / Math.log(10));
	if (number >= 0) {
		return excel.mathTrig.ROUND(Math.floor(number / significance)
				* significance, precision);
	} else if (mode === 0 || mode === undefined) {
		return -excel.mathTrig.ROUND(Math.ceil(Math.abs(number) / significance)
				* significance, precision);
	}
	return -excel.mathTrig.ROUND(Math.floor(Math.abs(number) / significance)
			* significance, precision);
};

// Deprecated
excel.mathTrig.FLOOR.PRECISE = excel.mathTrig.FLOOR.MATH;

// adapted
// http://rosettacode.org/wiki/Greatest_common_divisor#JavaScript
excel.mathTrig.GCD = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var n = range.length;
	var r0 = range[0];
	var x = r0 < 0 ? -r0 : r0;
	for ( var i = 1; i < n; i++) {
		var ri = range[i];
		var y = ri < 0 ? -ri : ri;
		while (x && y) {
			if (x > y) {
				x %= y;
			} else {
				y %= x;
			}
		}
		x += y;
	}
	return x;
};

excel.mathTrig.INT = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.floor(number);
};

// TODO: verify
excel.mathTrig.ISO = {
	CEILING : excel.mathTrig.CEILING
};

excel.mathTrig.LCM = function() {
	// Credits: Jonas Raoni Soares Silva
	var o = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (o instanceof Error) {
		return o;
	}
	for ( var i, j, n, d, r = 1; (n = o.pop()) !== undefined;) {
		while (n > 1) {
			if (n % 2) {
				for (i = 3, j = Math.floor(Math.sqrt(n)); i <= j && n % i; i += 2) {
					// empty
				}
				d = (i <= j) ? i : n;
			} else {
				d = 2;
			}
			for (n /= d, r *= d, i = o.length; i; (o[--i] % d) === 0
					&& (o[i] /= d) === 1 && o.splice(i, 1)) {
				// empty
			}
		}
	}
	return r;
};

excel.mathTrig.LN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.log(number);
};

excel.mathTrig.LOG = function(number, base) {
	number = excel.utils.parseNumber(number);
	base = excel.utils.parseNumber(base);
	if (excel.utils.anyIsError(number, base)) {
		return excel.error.value;
	}
	base = (base === undefined) ? 10 : base;
	return Math.log(number) / Math.log(base);
};

excel.mathTrig.LOG10 = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.log(number) / Math.log(10);
};

excel.mathTrig.MDETERM = function(matrix) {
	matrix = excel.utils.parseMatrix(matrix);
	if (matrix instanceof Error) {
		return matrix;
	}
	return numeric.det(matrix);
};

excel.mathTrig.MINVERSE = function(matrix) {
	matrix = excel.utils.parseMatrix(matrix);
	if (matrix instanceof Error) {
		return matrix;
	}
	return numeric.inv(matrix);
};

excel.mathTrig.MMULT = function(matrix1, matrix2) {
	matrix1 = excel.utils.parseMatrix(matrix1);
	matrix2 = excel.utils.parseMatrix(matrix2);
	if (excel.utils.anyIsError(matrix1, matrix2)) {
		return excel.error.value;
	}
	return numeric.dot(matrix1, matrix2);
};

excel.mathTrig.MOD = function(dividend, divisor) {
	dividend = excel.utils.parseNumber(dividend);
	divisor = excel.utils.parseNumber(divisor);
	if (excel.utils.anyIsError(dividend, divisor)) {
		return excel.error.value;
	}
	if (divisor === 0) {
		return excel.error.div0;
	}
	var modulus = Math.abs(dividend % divisor);
	return (divisor > 0) ? modulus : -modulus;
};

excel.mathTrig.MROUND = function(number, multiple) {
	number = excel.utils.parseNumber(number);
	multiple = excel.utils.parseNumber(multiple);
	if (excel.utils.anyIsError(number, multiple)) {
		return excel.error.value;
	}
	if (number * multiple < 0) {
		return excel.error.num;
	}

	return Math.round(number / multiple) * multiple;
};

excel.mathTrig.MULTINOMIAL = function() {
	var args = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (args instanceof Error) {
		return args;
	}
	var sum = 0;
	var divisor = 1;
	for ( var i = 0; i < args.length; i++) {
		sum += args[i];
		divisor *= excel.mathTrig.FACT(args[i]);
	}
	return excel.mathTrig.FACT(sum) / divisor;
};

excel.mathTrig.MUNIT = function(dimension) {
	dimension = excel.utils.parseNumber(dimension);
	if (dimension instanceof Error) {
		return dimension;
	}
	return numeric.identity(dimension);
};

excel.mathTrig.ODD = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	var temp = Math.ceil(Math.abs(number));
	temp = (temp & 1) ? temp : temp + 1;
	return (number > 0) ? temp : -temp;
};

excel.mathTrig.PI = function() {
	return Math.PI;
};

excel.mathTrig.POWER = function(number, power) {
	number = excel.utils.parseNumber(number);
	power = excel.utils.parseNumber(power);
	if (excel.utils.anyIsError(number, power)) {
		return excel.error.value;
	}
	var result = Math.pow(number, power);
	if (isNaN(result)) {
		return excel.error.num;
	}

	return result;
};

excel.mathTrig.PRODUCT = function() {
	var args = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (args instanceof Error) {
		return args;
	}
	var result = 1;
	for ( var i = 0; i < args.length; i++) {
		result *= args[i];
	}
	return result;
};

excel.mathTrig.QUOTIENT = function(numerator, denominator) {
	numerator = excel.utils.parseNumber(numerator);
	denominator = excel.utils.parseNumber(denominator);
	if (excel.utils.anyIsError(numerator, denominator)) {
		return excel.error.value;
	}
	return parseInt(numerator / denominator, 10);
};

excel.mathTrig.RADIANS = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return number * Math.PI / 180;
};

excel.mathTrig.RAND = function() {
	return Math.random();
};

excel.mathTrig.RANDBETWEEN = function(bottom, top) {
	bottom = excel.utils.parseNumber(bottom);
	top = excel.utils.parseNumber(top);
	if (excel.utils.anyIsError(bottom, top)) {
		return excel.error.value;
	}
	// Creative Commons Attribution 3.0 License
	// Copyright (c) 2012 eqcode
	return bottom + Math.ceil((top - bottom + 1) * Math.random()) - 1;
};

// TODO
excel.mathTrig.ROMAN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	// The MIT License
	// Copyright (c) 2008 Steven Levithan
	var digits = String(number).split('');
	var key = [ '', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM', '',
			'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC', '', 'I',
			'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX' ];
	var roman = '';
	var i = 3;
	while (i--) {
		roman = (key[+digits.pop() + (i * 10)] || '') + roman;
	}
	return new Array(+digits.join('') + 1).join('M') + roman;
};

excel.mathTrig.ROUND = function(number, digits) {
	number = excel.utils.parseNumber(number);
	digits = excel.utils.parseNumber(digits);
	if (excel.utils.anyIsError(number, digits)) {
		return excel.error.value;
	}
	return Math.round(number * Math.pow(10, digits)) / Math.pow(10, digits);
};

excel.mathTrig.ROUNDDOWN = function(number, digits) {
	number = excel.utils.parseNumber(number);
	digits = excel.utils.parseNumber(digits);
	if (excel.utils.anyIsError(number, digits)) {
		return excel.error.value;
	}
	var sign = (number > 0) ? 1 : -1;
	return sign * (Math.floor(Math.abs(number) * Math.pow(10, digits)))
			/ Math.pow(10, digits);
};

excel.mathTrig.ROUNDUP = function(number, digits) {
	number = excel.utils.parseNumber(number);
	digits = excel.utils.parseNumber(digits);
	if (excel.utils.anyIsError(number, digits)) {
		return excel.error.value;
	}
	var sign = (number > 0) ? 1 : -1;
	return sign * (Math.ceil(Math.abs(number) * Math.pow(10, digits)))
			/ Math.pow(10, digits);
};

excel.mathTrig.SEC = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return 1 / Math.cos(number);
};

excel.mathTrig.SECH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return 2 / (Math.exp(number) + Math.exp(-number));
};

excel.mathTrig.SERIESSUM = function(x, n, m, coefficients) {
	x = excel.utils.parseNumber(x);
	n = excel.utils.parseNumber(n);
	m = excel.utils.parseNumber(m);
	coefficients = excel.utils.parseNumberArray(coefficients);
	if (excel.utils.anyIsError(x, n, m, coefficients)) {
		return excel.error.value;
	}
	var result = coefficients[0] * Math.pow(x, n);
	for ( var i = 1; i < coefficients.length; i++) {
		result += coefficients[i] * Math.pow(x, n + i * m);
	}
	return result;
};

excel.mathTrig.SIGN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	if (number < 0) {
		return -1;
	} else if (number === 0) {
		return 0;
	} else {
		return 1;
	}
};

excel.mathTrig.SIN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.sin(number);
};

excel.mathTrig.SINH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return (Math.exp(number) - Math.exp(-number)) / 2;
};

excel.mathTrig.SQRT = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	if (number < 0) {
		return excel.error.num;
	}
	return Math.sqrt(number);
};

excel.mathTrig.SQRTPI = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.sqrt(number * Math.PI);
};

excel.mathTrig.SUBTOTAL = function(function_code, ref1) {
	function_code = excel.utils.parseNumber(function_code);
	if (function_code instanceof Error) {
		return function_code;
	}
	switch (function_code) {
	case 1:
		return excel.statistical.AVERAGE(ref1);
	case 2:
		return excel.statistical.COUNT(ref1);
	case 3:
		return excel.statistical.COUNTA(ref1);
	case 4:
		return excel.statistical.MAX(ref1);
	case 5:
		return excel.statistical.MIN(ref1);
	case 6:
		return excel.mathTrig.PRODUCT(ref1);
	case 7:
		return excel.statistical.STDEV.S(ref1);
	case 8:
		return excel.statistical.STDEV.P(ref1);
	case 9:
		return excel.mathTrig.SUM(ref1);
	case 10:
		return excel.statistical.VAR.S(ref1);
	case 11:
		return excel.statistical.VAR.P(ref1);
		// no hidden values for us
	case 101:
		return excel.statistical.AVERAGE(ref1);
	case 102:
		return excel.statistical.COUNT(ref1);
	case 103:
		return excel.statistical.COUNTA(ref1);
	case 104:
		return excel.statistical.MAX(ref1);
	case 105:
		return excel.statistical.MIN(ref1);
	case 106:
		return excel.mathTrig.PRODUCT(ref1);
	case 107:
		return excel.statistical.STDEV.S(ref1);
	case 108:
		return excel.statistical.STDEV.P(ref1);
	case 109:
		return excel.mathTrig.SUM(ref1);
	case 110:
		return excel.statistical.VAR.S(ref1);
	case 111:
		return excel.statistical.VAR.P(ref1);

	}
};

excel.mathTrig.ADD = function(num1, num2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	num1 = excel.utils.parseNumber(num1);
	num2 = excel.utils.parseNumber(num2);
	if (excel.utils.anyIsError(num1, num2)) {
		return excel.error.value;
	}

	return num1 + num2;
};

excel.mathTrig.MINUS = function(num1, num2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	num1 = excel.utils.parseNumber(num1);
	num2 = excel.utils.parseNumber(num2);
	if (excel.utils.anyIsError(num1, num2)) {
		return excel.error.value;
	}

	return num1 - num2;
};

excel.mathTrig.DIVIDE = function(dividend, divisor) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	dividend = excel.utils.parseNumber(dividend);
	divisor = excel.utils.parseNumber(divisor);
	if (excel.utils.anyIsError(dividend, divisor)) {
		return excel.error.value;
	}

	if (divisor === 0) {
		return excel.error.div0;
	}

	return dividend / divisor;
};

excel.mathTrig.MULTIPLY = function(factor1, factor2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	factor1 = excel.utils.parseNumber(factor1);
	factor2 = excel.utils.parseNumber(factor2);
	if (excel.utils.anyIsError(factor1, factor2)) {
		return excel.error.value;
	}

	return factor1 * factor2;
};

excel.mathTrig.GTE = function(num1, num2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	num1 = excel.utils.parseNumber(num1);
	num2 = excel.utils.parseNumber(num2);
	if (excel.utils.anyIsError(num1, num2)) {
		return excel.error.error;
	}

	return num1 >= num2;
};

excel.mathTrig.LT = function(num1, num2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	num1 = excel.utils.parseNumber(num1);
	num2 = excel.utils.parseNumber(num2);
	if (excel.utils.anyIsError(num1, num2)) {
		return excel.error.error;
	}

	return num1 < num2;
};

excel.mathTrig.LTE = function(num1, num2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	num1 = excel.utils.parseNumber(num1);
	num2 = excel.utils.parseNumber(num2);
	if (excel.utils.anyIsError(num1, num2)) {
		return excel.error.error;
	}

	return num1 <= num2;
};

excel.mathTrig.EQ = function(value1, value2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	return value1 === value2;
};

excel.mathTrig.NE = function(value1, value2) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	return value1 !== value2;
};

excel.mathTrig.POW = function(base, exponent) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	base = excel.utils.parseNumber(base);
	exponent = excel.utils.parseNumber(exponent);
	if (excel.utils.anyIsError(base, exponent)) {
		return excel.error.error;
	}

	return excel.mathTrig.POWER(base, exponent);
};

excel.mathTrig.SUM = function() {
	var result = 0;
	var argsKeys = Object.keys(arguments);
	for ( var i = 0; i < argsKeys.length; ++i) {
		var elt = arguments[argsKeys[i]];
		if (typeof elt === 'number') {
			result += elt;
		} else if (typeof elt === 'string') {
			var parsed = parseFloat(elt);
			!isNaN(parsed) && (result += parsed);
		} else if (Array.isArray(elt)) {
			result += excel.mathTrig.SUM.apply(null, elt);
		}
	}
	return result;
};

excel.mathTrig.SUMIF = function(range, criteria) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	if (range instanceof Error) {
		return range;
	}
	var result = 0;
	for ( var i = 0; i < range.length; i++) {
		result += (eval(range[i] + criteria)) ? range[i] : 0;
	}
	return result;
};

excel.mathTrig.SUMIFS = function() {
	var args = excel.utils.argsToArray(arguments);
	var range = excel.utils.parseNumberArray(excel.utils.flatten(args.shift()));
	if (range instanceof Error) {
		return range;
	}
	var criteria = args;

	var n_range_elements = range.length;
	var n_criterias = criteria.length;

	var result = 0;
	for ( var i = 0; i < n_range_elements; i++) {
		var el = range[i];
		var condition = '';
		for ( var c = 0; c < n_criterias; c++) {
			condition += el + criteria[c];
			if (c !== n_criterias - 1) {
				condition += '&&';
			}
		}
		if (eval(condition)) {
			result += el;
		}
	}
	return result;
};

excel.mathTrig.SUMPRODUCT = function() {
	if (!arguments || arguments.length === 0) {
		return excel.error.value;
	}
	var arrays = arguments.length + 1;
	var result = 0;
	var product;
	var k;
	var _i;
	var _ij;
	for ( var i = 0; i < arguments[0].length; i++) {
		if (!(arguments[0][i] instanceof Array)) {
			product = 1;
			for (k = 1; k < arrays; k++) {
				_i = excel.utils.parseNumber(arguments[k - 1][i]);
				if (_i instanceof Error) {
					return _i;
				}
				product *= _i;
			}
			result += product;
		} else {
			for ( var j = 0; j < arguments[0][i].length; j++) {
				product = 1;
				for (k = 1; k < arrays; k++) {
					_ij = excel.utils.parseNumber(arguments[k - 1][i][j]);
					if (_ij instanceof Error) {
						return _ij;
					}
					product *= _ij;
				}
				result += product;
			}
		}
	}
	return result;
};

excel.mathTrig.SUMSQ = function() {
	var numbers = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (numbers instanceof Error) {
		return numbers;
	}
	var result = 0;
	var length = numbers.length;
	for ( var i = 0; i < length; i++) {
		result += (excel.information.ISNUMBER(numbers[i])) ? numbers[i]
				* numbers[i] : 0;
	}
	return result;
};

excel.mathTrig.SUMX2MY2 = function(array_x, array_y) {
	array_x = excel.utils.parseNumberArray(excel.utils.flatten(array_x));
	array_y = excel.utils.parseNumberArray(excel.utils.flatten(array_y));
	if (excel.utils.anyIsError(array_x, array_y)) {
		return excel.error.value;
	}
	var result = 0;
	for ( var i = 0; i < array_x.length; i++) {
		result += array_x[i] * array_x[i] - array_y[i] * array_y[i];
	}
	return result;
};

excel.mathTrig.SUMX2PY2 = function(array_x, array_y) {
	array_x = excel.utils.parseNumberArray(excel.utils.flatten(array_x));
	array_y = excel.utils.parseNumberArray(excel.utils.flatten(array_y));
	if (excel.utils.anyIsError(array_x, array_y)) {
		return excel.error.value;
	}
	var result = 0;
	array_x = excel.utils.parseNumberArray(excel.utils.flatten(array_x));
	array_y = excel.utils.parseNumberArray(excel.utils.flatten(array_y));
	for ( var i = 0; i < array_x.length; i++) {
		result += array_x[i] * array_x[i] + array_y[i] * array_y[i];
	}
	return result;
};

excel.mathTrig.SUMXMY2 = function(array_x, array_y) {
	array_x = excel.utils.parseNumberArray(excel.utils.flatten(array_x));
	array_y = excel.utils.parseNumberArray(excel.utils.flatten(array_y));
	if (excel.utils.anyIsError(array_x, array_y)) {
		return excel.error.value;
	}
	var result = 0;
	array_x = excel.utils.flatten(array_x);
	array_y = excel.utils.flatten(array_y);
	for ( var i = 0; i < array_x.length; i++) {
		result += Math.pow(array_x[i] - array_y[i], 2);
	}
	return result;
};

excel.mathTrig.TAN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return Math.tan(number);
};

excel.mathTrig.TANH = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	var e2 = Math.exp(2 * number);
	return (e2 - 1) / (e2 + 1);
};

excel.mathTrig.TRUNC = function(number, digits) {
	digits = (digits === undefined) ? 0 : digits;
	number = excel.utils.parseNumber(number);
	digits = excel.utils.parseNumber(digits);
	if (excel.utils.anyIsError(number, digits)) {
		return excel.error.value;
	}
	var sign = (number > 0) ? 1 : -1;
	return sign * (Math.floor(Math.abs(number) * Math.pow(10, digits)))
			/ Math.pow(10, digits);
};
