excel.information = {};
excel.information.CELL = function() {
	throw new Error('CELL is not implemented');
};

excel.information.ERROR = {};
excel.information.ERROR.TYPE = function(error_val) {
	switch (error_val) {
	case excel.error.nil:
		return 1;
	case excel.error.div0:
		return 2;
	case excel.error.value:
		return 3;
	case excel.error.ref:
		return 4;
	case excel.error.name:
		return 5;
	case excel.error.num:
		return 6;
	case excel.error.na:
		return 7;
	case excel.error.data:
		return 8;
	}
	return excel.error.na;
};

// TODO
excel.information.INFO = function() {
	throw new Error('INFO is not implemented');
};

excel.information.ISBLANK = function(value) {
	return value === null;
};

excel.information.ISBINARY = function(number) {
	return (/^[01]{1,10}$/).test(number);
};

excel.information.ISERR = function(value) {
	return ([ excel.error.value, excel.error.ref, excel.error.div0,
			excel.error.num, excel.error.name, excel.error.nil ])
			.indexOf(value) >= 0
			|| (typeof value === 'number' && (isNaN(value) || !isFinite(value)));
};

excel.information.ISERROR = function(value) {
	return excel.information.ISERR(value) || value === excel.error.na;
};

excel.information.ISEVEN = function(number) {
	return (Math.floor(Math.abs(number)) & 1) ? false : true;
};

// TODO
excel.information.ISFORMULA = function() {
	throw new Error('ISFORMULA is not implemented');
};

excel.information.ISLOGICAL = function(value) {
	return value === true || value === false;
};

excel.information.ISNA = function(value) {
	return value === excel.error.na;
};

excel.information.ISNONTEXT = function(value) {
	return typeof (value) !== 'string';
};

excel.information.ISNUMBER = function(value) {
	return typeof (value) === 'number' && !isNaN(value) && isFinite(value);
};

excel.information.ISODD = function(number) {
	return (Math.floor(Math.abs(number)) & 1) ? true : false;
};

// TODO
excel.information.ISREF = function() {
	throw new Error('ISREF is not implemented');
};

excel.information.ISTEXT = function(value) {
	return typeof (value) === 'string';
};

excel.information.N = function(value) {
	if (this.ISNUMBER(value)) {
		return value;
	}
	if (value instanceof Date) {
		return value.getTime();
	}
	if (value === true) {
		return 1;
	}
	if (value === false) {
		return 0;
	}
	if (this.ISERROR(value)) {
		return value;
	}
	return 0;
};

excel.information.NA = function() {
	return excel.error.na;
};

// TODO
excel.information.SHEET = function() {
	throw new Error('SHEET is not implemented');
};

// TODO
excel.information.SHEETS = function() {
	throw new Error('SHEETS is not implemented');
};

excel.information.TYPE = function(value) {
	if (this.ISNUMBER(value)) {
		return 1;
	}
	if (this.ISTEXT(value)) {
		return 2;
	}
	if (this.ISLOGICAL(value)) {
		return 4;
	}
	if (this.ISERROR(value)) {
		return 16;
	}
	if (Array.isArray(value)) {
		return 64;
	}
};
