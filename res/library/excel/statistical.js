var SQRT2PI = 2.5066282746310002;
excel.statistical = {};
excel.statistical.AVEDEV = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	return jStat.sum(jStat(range).subtract(jStat.mean(range)).abs()[0])
			/ range.length;
};

excel.statistical.AVERAGE = function() {
	var range = excel.utils.numbers(excel.utils.flatten(arguments));
	var n = range.length;
	var sum = 0;
	var count = 0;
	for ( var i = 0; i < n; i++) {
		sum += range[i];
		count += 1;
	}
	return sum / count;
};

excel.statistical.AVERAGEA = function() {
	var range = excel.utils.flatten(arguments);
	var n = range.length;
	var sum = 0;
	var count = 0;
	for ( var i = 0; i < n; i++) {
		var el = range[i];
		if (typeof el === 'number') {
			sum += el;
		}
		if (el === true) {
			sum++;
		}
		if (el !== null) {
			count++;
		}
	}
	return sum / count;
};

excel.statistical.AVERAGEIF = function(range, criteria, average_range) {
	average_range = average_range || range;
	range = excel.utils.flatten(range);
	average_range = excel.utils.parseNumberArray(excel.utils
			.flatten(average_range));
	if (average_range instanceof Error) {
		return average_range;
	}
	var average_count = 0;
	var result = 0;
	for ( var i = 0; i < range.length; i++) {
		if (eval(range[i] + criteria)) {
			result += average_range[i];
			average_count++;
		}
	}
	return result / average_count;
};

excel.statistical.AVERAGEIFS = function() {
	// Does not work with multi dimensional ranges yet!
	// http://office.microsoft.com/en-001/excel-help/averageifs-function-HA010047493.aspx
	var args = excel.utils.argsToArray(arguments);
	var criteria = (args.length - 1) / 2;
	var range = excel.utils.flatten(args[0]);
	var count = 0;
	var result = 0;
	for ( var i = 0; i < range.length; i++) {
		var condition = '';
		for ( var j = 0; j < criteria; j++) {
			condition += args[2 * j + 1][i] + args[2 * j + 2];
			if (j !== criteria - 1) {
				condition += '&&';
			}
		}
		if (eval(condition)) {
			result += range[i];
			count++;
		}
	}

	var average = result / count;
	if (isNaN(average)) {
		return 0;
	} else {
		return average;
	}
};

excel.statistical.BETA = {};

excel.statistical.BETA.DIST = function(x, alpha, beta, cumulative, A, B) {
	A = (A === undefined) ? 0 : A;
	B = (B === undefined) ? 1 : B;

	x = excel.utils.parseNumber(x);
	alpha = excel.utils.parseNumber(alpha);
	beta = excel.utils.parseNumber(beta);
	A = excel.utils.parseNumber(A);
	B = excel.utils.parseNumber(B);
	if (excel.utils.anyIsError(x, alpha, beta, A, B)) {
		return excel.error.value;
	}

	x = (x - A) / (B - A);
	return (cumulative) ? jStat.beta.cdf(x, alpha, beta) : jStat.beta.pdf(x,
			alpha, beta);
};

excel.statistical.BETA.INV = function(probability, alpha, beta, A, B) {
	A = (A === undefined) ? 0 : A;
	B = (B === undefined) ? 1 : B;

	probability = excel.utils.parseNumber(probability);
	alpha = excel.utils.parseNumber(alpha);
	beta = excel.utils.parseNumber(beta);
	A = excel.utils.parseNumber(A);
	B = excel.utils.parseNumber(B);
	if (excel.utils.anyIsError(probability, alpha, beta, A, B)) {
		return excel.error.value;
	}

	return jStat.beta.inv(probability, alpha, beta) * (B - A) + A;
};

excel.statistical.BINOM = {};

excel.statistical.BINOM.DIST = function(successes, trials, probability,
		cumulative) {
	successes = excel.utils.parseNumber(successes);
	trials = excel.utils.parseNumber(trials);
	probability = excel.utils.parseNumber(probability);
	cumulative = excel.utils.parseNumber(cumulative);
	if (excel.utils.anyIsError(successes, trials, probability, cumulative)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.binomial.cdf(successes, trials, probability)
			: jStat.binomial.pdf(successes, trials, probability);
};

excel.statistical.BINOM.DIST.RANGE = function(trials, probability, successes,
		successes2) {
	successes2 = (successes2 === undefined) ? successes : successes2;

	trials = excel.utils.parseNumber(trials);
	probability = excel.utils.parseNumber(probability);
	successes = excel.utils.parseNumber(successes);
	successes2 = excel.utils.parseNumber(successes2);
	if (excel.utils.anyIsError(trials, probability, successes, successes2)) {
		return excel.error.value;
	}

	var result = 0;
	for ( var i = successes; i <= successes2; i++) {
		result += excel.mathTrig.COMBIN(trials, i) * Math.pow(probability, i)
				* Math.pow(1 - probability, trials - i);
	}
	return result;
};

excel.statistical.BINOM.INV = function(trials, probability, alpha) {
	trials = excel.utils.parseNumber(trials);
	probability = excel.utils.parseNumber(probability);
	alpha = excel.utils.parseNumber(alpha);
	if (excel.utils.anyIsError(trials, probability, alpha)) {
		return excel.error.value;
	}

	var x = 0;
	while (x <= trials) {
		if (jStat.binomial.cdf(x, trials, probability) >= alpha) {
			return x;
		}
		x++;
	}
};

excel.statistical.CHISQ = {};

excel.statistical.CHISQ.DIST = function(x, k, cumulative) {
	x = excel.utils.parseNumber(x);
	k = excel.utils.parseNumber(k);
	if (excel.utils.anyIsError(x, k)) {
		return excel.error.value;
	}

	return (cumulative) ? jStat.chisquare.cdf(x, k) : jStat.chisquare.pdf(x, k);
};

excel.statistical.CHISQ.DIST.RT = function(x, k) {
	if (!x | !k) {
		return excel.error.na;
	}

	if (x < 1 || k > Math.pow(10, 10)) {
		return excel.error.num;
	}

	if ((typeof x !== 'number') || (typeof k !== 'number')) {
		return excel.error.value;
	}

	return 1 - jStat.chisquare.cdf(x, k);
};

excel.statistical.CHISQ.INV = function(probability, k) {
	probability = excel.utils.parseNumber(probability);
	k = excel.utils.parseNumber(k);
	if (excel.utils.anyIsError(probability, k)) {
		return excel.error.value;
	}
	return jStat.chisquare.inv(probability, k);
};

excel.statistical.CHISQ.INV.RT = function(p, k) {
	if (!p | !k) {
		return excel.error.na;
	}

	if (p < 0 || p > 1 || k < 1 || k > Math.pow(10, 10)) {
		return excel.error.num;
	}

	if ((typeof p !== 'number') || (typeof k !== 'number')) {
		return excel.error.value;
	}

	return jStat.chisquare.inv(1.0 - p, k);
};

excel.statistical.CHISQ.TEST = function(observed, expected) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	if ((!(observed instanceof Array)) || (!(expected instanceof Array))) {
		return excel.error.value;
	}

	if (observed.length !== expected.length) {
		return excel.error.value;
	}

	if (observed[0] && expected[0] && observed[0].length !== expected[0].length) {
		return excel.error.value;
	}

	var row = observed.length;
	var tmp, i, j;

	// Convert single-dimension array into two-dimension array
	for (i = 0; i < row; i++) {
		if (!(observed[i] instanceof Array)) {
			tmp = observed[i];
			observed[i] = [];
			observed[i].push(tmp);
		}
		if (!(expected[i] instanceof Array)) {
			tmp = expected[i];
			expected[i] = [];
			expected[i].push(tmp);
		}
	}

	var col = observed[0].length;
	var dof = (col === 1) ? row - 1 : (row - 1) * (col - 1);
	var xsqr = 0;
	var Pi = Math.PI;

	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			xsqr += Math.pow((observed[i][j] - expected[i][j]), 2)
					/ expected[i][j];
		}
	}

	// Get independency by X square and its degree of freedom
	function ChiSq(xsqr, dof) {
		var p = Math.exp(-0.5 * xsqr);
		if ((dof % 2) === 1) {
			p = p * Math.sqrt(2 * xsqr / Pi);
		}
		var k = dof;
		while (k >= 2) {
			p = p * xsqr / k;
			k = k - 2;
		}
		var t = p;
		var a = dof;
		while (t > 0.0000000001 * p) {
			a = a + 2;
			t = t * xsqr / a;
			p = p + t;
		}
		return 1 - p;
	}

	return Math.round(ChiSq(xsqr, dof) * 1000000) / 1000000;
};

excel.statistical.COLUMN = function(matrix, index) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	if (index < 0) {
		return excel.error.num;
	}

	if (!(matrix instanceof Array) || (typeof index !== 'number')) {
		return excel.error.value;
	}

	if (matrix.length === 0) {
		return undefined;
	}

	return jStat.col(matrix, index);
};

excel.statistical.COLUMNS = function(matrix) {
	if (arguments.length !== 1) {
		return excel.error.na;
	}

	if (!(matrix instanceof Array)) {
		return excel.error.value;
	}

	if (matrix.length === 0) {
		return 0;
	}

	return jStat.cols(matrix);
};

excel.statistical.CONFIDENCE = {};

excel.statistical.CONFIDENCE.NORM = function(alpha, sd, n) {
	alpha = excel.utils.parseNumber(alpha);
	sd = excel.utils.parseNumber(sd);
	n = excel.utils.parseNumber(n);
	if (excel.utils.anyIsError(alpha, sd, n)) {
		return excel.error.value;
	}
	return jStat.normalci(1, alpha, sd, n)[1] - 1;
};

excel.statistical.CONFIDENCE.T = function(alpha, sd, n) {
	alpha = excel.utils.parseNumber(alpha);
	sd = excel.utils.parseNumber(sd);
	n = excel.utils.parseNumber(n);
	if (excel.utils.anyIsError(alpha, sd, n)) {
		return excel.error.value;
	}
	return jStat.tci(1, alpha, sd, n)[1] - 1;
};

excel.statistical.CORREL = function(array1, array2) {
	array1 = excel.utils.parseNumberArray(excel.utils.flatten(array1));
	array2 = excel.utils.parseNumberArray(excel.utils.flatten(array2));
	if (excel.utils.anyIsError(array1, array2)) {
		return excel.error.value;
	}
	return jStat.corrcoeff(array1, array2);
};

excel.statistical.COUNT = function() {
	return excel.utils.numbers(excel.utils.flatten(arguments)).length;
};

excel.statistical.COUNTA = function() {
	var range = excel.utils.flatten(arguments);
	return range.length - excel.statistical.COUNTBLANK(range);
};

excel.statistical.COUNTIN = function(range, value) {
	var result = 0;
	for ( var i = 0; i < range.length; i++) {
		if (range[i] === value) {
			result++;
		}
	}
	return result;
};

excel.statistical.COUNTBLANK = function() {
	var range = excel.utils.flatten(arguments);
	var blanks = 0;
	var element;
	for ( var i = 0; i < range.length; i++) {
		element = range[i];
		if (element === null || element === '') {
			blanks++;
		}
	}
	return blanks;
};

excel.statistical.COUNTIF = function(range, criteria) {
	range = excel.utils.flatten(range);
	if (!/[<>=!]/.test(criteria)) {
		criteria = '=="' + criteria + '"';
	}
	var matches = 0;
	for ( var i = 0; i < range.length; i++) {
		if (typeof range[i] !== 'string') {
			if (eval(range[i] + criteria)) {
				matches++;
			}
		} else {
			if (eval('"' + range[i] + '"' + criteria)) {
				matches++;
			}
		}
	}
	return matches;
};

excel.statistical.COUNTIFS = function() {
	var args = excel.utils.argsToArray(arguments);
	var results = new Array(excel.utils.flatten(args[0]).length);
	for ( var i = 0; i < results.length; i++) {
		results[i] = true;
	}
	for (i = 0; i < args.length; i += 2) {
		var range = excel.utils.flatten(args[i]);
		var criteria = args[i + 1];
		if (!/[<>=!]/.test(criteria)) {
			criteria = '=="' + criteria + '"';
		}
		for ( var j = 0; j < range.length; j++) {
			if (typeof range[j] !== 'string') {
				results[j] = results[j] && eval(range[j] + criteria);
			} else {
				results[j] = results[j]
						&& eval('"' + range[j] + '"' + criteria);
			}
		}
	}
	var result = 0;
	for (i = 0; i < results.length; i++) {
		if (results[i]) {
			result++;
		}
	}
	return result;
};

excel.statistical.COUNTUNIQUE = function() {
	return excel.miscellaneous.UNIQUE.apply(null, excel.utils
			.flatten(arguments)).length;
};

excel.statistical.COVARIANCE = {};

excel.statistical.COVARIANCE.P = function(array1, array2) {
	array1 = excel.utils.parseNumberArray(excel.utils.flatten(array1));
	array2 = excel.utils.parseNumberArray(excel.utils.flatten(array2));
	if (excel.utils.anyIsError(array1, array2)) {
		return excel.error.value;
	}
	var mean1 = jStat.mean(array1);
	var mean2 = jStat.mean(array2);
	var result = 0;
	var n = array1.length;
	for ( var i = 0; i < n; i++) {
		result += (array1[i] - mean1) * (array2[i] - mean2);
	}
	return result / n;
};

excel.statistical.COVARIANCE.S = function(array1, array2) {
	array1 = excel.utils.parseNumberArray(excel.utils.flatten(array1));
	array2 = excel.utils.parseNumberArray(excel.utils.flatten(array2));
	if (excel.utils.anyIsError(array1, array2)) {
		return excel.error.value;
	}
	return jStat.covariance(array1, array2);
};

excel.statistical.DEVSQ = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var mean = jStat.mean(range);
	var result = 0;
	for ( var i = 0; i < range.length; i++) {
		result += Math.pow((range[i] - mean), 2);
	}
	return result;
};

excel.statistical.EXPON = {};

excel.statistical.EXPON.DIST = function(x, lambda, cumulative) {
	x = excel.utils.parseNumber(x);
	lambda = excel.utils.parseNumber(lambda);
	if (excel.utils.anyIsError(x, lambda)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.exponential.cdf(x, lambda) : jStat.exponential
			.pdf(x, lambda);
};

excel.statistical.F = {};

excel.statistical.F.DIST = function(x, d1, d2, cumulative) {
	x = excel.utils.parseNumber(x);
	d1 = excel.utils.parseNumber(d1);
	d2 = excel.utils.parseNumber(d2);
	if (excel.utils.anyIsError(x, d1, d2)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.centralF.cdf(x, d1, d2) : jStat.centralF.pdf(x,
			d1, d2);
};

excel.statistical.F.DIST.RT = function(x, d1, d2) {
	if (arguments.length !== 3) {
		return excel.error.na;
	}

	if (x < 0 || d1 < 1 || d2 < 1) {
		return excel.error.num;
	}

	if ((typeof x !== 'number') || (typeof d1 !== 'number')
			|| (typeof d2 !== 'number')) {
		return excel.error.value;
	}

	return 1 - jStat.centralF.cdf(x, d1, d2);
};

excel.statistical.F.INV = function(probability, d1, d2) {
	probability = excel.utils.parseNumber(probability);
	d1 = excel.utils.parseNumber(d1);
	d2 = excel.utils.parseNumber(d2);
	if (excel.utils.anyIsError(probability, d1, d2)) {
		return excel.error.value;
	}
	if (probability <= 0.0 || probability > 1.0) {
		return excel.error.num;
	}

	return jStat.centralF.inv(probability, d1, d2);
};

excel.statistical.F.INV.RT = function(p, d1, d2) {
	if (arguments.length !== 3) {
		return excel.error.na;
	}

	if (p < 0 || p > 1 || d1 < 1 || d1 > Math.pow(10, 10) || d2 < 1
			|| d2 > Math.pow(10, 10)) {
		return excel.error.num;
	}

	if ((typeof p !== 'number') || (typeof d1 !== 'number')
			|| (typeof d2 !== 'number')) {
		return excel.error.value;
	}

	return jStat.centralF.inv(1.0 - p, d1, d2)
};

excel.statistical.F.TEST = function(array1, array2) {
	if (!array1 || !array2) {
		return excel.error.na;
	}

	if (!(array1 instanceof Array) || !(array2 instanceof Array)) {
		return excel.error.na;
	}

	if (array1.length < 2 || array2.length < 2) {
		return excel.error.div0;
	}

	var sumOfSquares = function(values, x1) {
		var sum = 0;
		for ( var i = 0; i < values.length; i++) {
			sum += Math.pow((values[i] - x1), 2);
		}
		return sum;
	};

	var x1 = excel.mathTrig.SUM(array1) / array1.length;
	var x2 = excel.mathTrig.SUM(array2) / array2.length;
	var sum1 = sumOfSquares(array1, x1) / (array1.length - 1);
	var sum2 = sumOfSquares(array2, x2) / (array2.length - 1);

	return sum1 / sum2;
};

excel.statistical.FISHER = function(x) {
	x = excel.utils.parseNumber(x);
	if (x instanceof Error) {
		return x;
	}
	return Math.log((1 + x) / (1 - x)) / 2;
};

excel.statistical.FISHERINV = function(y) {
	y = excel.utils.parseNumber(y);
	if (y instanceof Error) {
		return y;
	}
	var e2y = Math.exp(2 * y);
	return (e2y - 1) / (e2y + 1);
};

excel.statistical.FORECAST = function(x, data_y, data_x) {
	x = excel.utils.parseNumber(x);
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	if (excel.utils.anyIsError(x, data_y, data_x)) {
		return excel.error.value;
	}
	var xmean = jStat.mean(data_x);
	var ymean = jStat.mean(data_y);
	var n = data_x.length;
	var num = 0;
	var den = 0;
	for ( var i = 0; i < n; i++) {
		num += (data_x[i] - xmean) * (data_y[i] - ymean);
		den += Math.pow(data_x[i] - xmean, 2);
	}
	var b = num / den;
	var a = ymean - b * xmean;
	return a + b * x;
};

excel.statistical.FREQUENCY = function(data, bins) {
	data = excel.utils.parseNumberArray(excel.utils.flatten(data));
	bins = excel.utils.parseNumberArray(excel.utils.flatten(bins));
	if (excel.utils.anyIsError(data, bins)) {
		return excel.error.value;
	}
	var n = data.length;
	var b = bins.length;
	var r = [];
	for ( var i = 0; i <= b; i++) {
		r[i] = 0;
		for ( var j = 0; j < n; j++) {
			if (i === 0) {
				if (data[j] <= bins[0]) {
					r[0] += 1;
				}
			} else if (i < b) {
				if (data[j] > bins[i - 1] && data[j] <= bins[i]) {
					r[i] += 1;
				}
			} else if (i === b) {
				if (data[j] > bins[b - 1]) {
					r[b] += 1;
				}
			}
		}
	}
	return r;
};

excel.statistical.GAMMA = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}

	if (number === 0) {
		return excel.error.num;
	}

	if (parseInt(number, 10) === number && number < 0) {
		return excel.error.num;
	}

	return jStat.gammafn(number);
};

excel.statistical.GAMMA.DIST = function(value, alpha, beta, cumulative) {
	if (arguments.length !== 4) {
		return excel.error.na;
	}

	if (value < 0 || alpha <= 0 || beta <= 0) {
		return excel.error.value;
	}

	if ((typeof value !== 'number') || (typeof alpha !== 'number')
			|| (typeof beta !== 'number')) {
		return excel.error.value;
	}

	return cumulative ? jStat.gamma.cdf(value, alpha, beta, true) : jStat.gamma
			.pdf(value, alpha, beta, false);
};

excel.statistical.GAMMA.INV = function(probability, alpha, beta) {
	if (arguments.length !== 3) {
		return excel.error.na;
	}

	if (probability < 0 || probability > 1 || alpha <= 0 || beta <= 0) {
		return excel.error.num;
	}

	if ((typeof probability !== 'number') || (typeof alpha !== 'number')
			|| (typeof beta !== 'number')) {
		return excel.error.value;
	}

	return jStat.gamma.inv(probability, alpha, beta);
};

excel.statistical.GAMMALN = function(number) {
	number = excel.utils.parseNumber(number);
	if (number instanceof Error) {
		return number;
	}
	return jStat.gammaln(number);
};

excel.statistical.GAMMALN.PRECISE = function(x) {
	if (arguments.length !== 1) {
		return excel.error.na;
	}

	if (x <= 0) {
		return excel.error.num;
	}

	if (typeof x !== 'number') {
		return excel.error.value;
	}

	return jStat.gammaln(x);
};

excel.statistical.GAUSS = function(z) {
	z = excel.utils.parseNumber(z);
	if (z instanceof Error) {
		return z;
	}
	return jStat.normal.cdf(z, 0, 1) - 0.5;
};

excel.statistical.GEOMEAN = function() {
	var args = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (args instanceof Error) {
		return args;
	}
	return jStat.geomean(args);
};

excel.statistical.GROWTH = function(known_y, known_x, new_x, use_const) {
	// Credits: Ilmari Karonen
	// (http://stackoverflow.com/questions/14161990/how-to-implement-growth-function-in-javascript)

	known_y = excel.utils.parseNumberArray(known_y);
	if (known_y instanceof Error) {
		return known_y;
	}

	// Default values for optional parameters:
	var i;
	if (known_x === undefined) {
		known_x = [];
		for (i = 1; i <= known_y.length; i++) {
			known_x.push(i);
		}
	}
	if (new_x === undefined) {
		new_x = [];
		for (i = 1; i <= known_y.length; i++) {
			new_x.push(i);
		}
	}

	known_x = excel.utils.parseNumberArray(known_x);
	new_x = excel.utils.parseNumberArray(new_x);
	if (excel.utils.anyIsError(known_x, new_x)) {
		return excel.error.value;
	}

	if (use_const === undefined) {
		use_const = true;
	}

	// Calculate sums over the data:
	var n = known_y.length;
	var avg_x = 0;
	var avg_y = 0;
	var avg_xy = 0;
	var avg_xx = 0;
	for (i = 0; i < n; i++) {
		var x = known_x[i];
		var y = Math.log(known_y[i]);
		avg_x += x;
		avg_y += y;
		avg_xy += x * y;
		avg_xx += x * x;
	}
	avg_x /= n;
	avg_y /= n;
	avg_xy /= n;
	avg_xx /= n;

	// Compute linear regression coefficients:
	var beta;
	var alpha;
	if (use_const) {
		beta = (avg_xy - avg_x * avg_y) / (avg_xx - avg_x * avg_x);
		alpha = avg_y - beta * avg_x;
	} else {
		beta = avg_xy / avg_xx;
		alpha = 0;
	}

	// Compute and return result array:
	var new_y = [];
	for (i = 0; i < new_x.length; i++) {
		new_y.push(Math.exp(alpha + beta * new_x[i]));
	}
	return new_y;
};

excel.statistical.HARMEAN = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var n = range.length;
	var den = 0;
	for ( var i = 0; i < n; i++) {
		den += 1 / range[i];
	}
	return n / den;
};

excel.statistical.HYPGEOM = {};

excel.statistical.HYPGEOM.DIST = function(x, n, M, N, cumulative) {
	x = excel.utils.parseNumber(x);
	n = excel.utils.parseNumber(n);
	M = excel.utils.parseNumber(M);
	N = excel.utils.parseNumber(N);
	if (excel.utils.anyIsError(x, n, M, N)) {
		return excel.error.value;
	}

	function pdf(x, n, M, N) {
		return excel.mathTrig.COMBIN(M, x)
				* excel.mathTrig.COMBIN(N - M, n - x)
				/ excel.mathTrig.COMBIN(N, n);
	}

	function cdf(x, n, M, N) {
		var result = 0;
		for ( var i = 0; i <= x; i++) {
			result += pdf(i, n, M, N);
		}
		return result;
	}

	return (cumulative) ? cdf(x, n, M, N) : pdf(x, n, M, N);
};

excel.statistical.INTERCEPT = function(known_y, known_x) {
	known_y = excel.utils.parseNumberArray(known_y);
	known_x = excel.utils.parseNumberArray(known_x);
	if (excel.utils.anyIsError(known_y, known_x)) {
		return excel.error.value;
	}
	if (known_y.length !== known_x.length) {
		return excel.error.na;
	}
	return excel.statistical.FORECAST(0, known_y, known_x);
};

excel.statistical.KURT = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var mean = jStat.mean(range);
	var n = range.length;
	var sigma = 0;
	for ( var i = 0; i < n; i++) {
		sigma += Math.pow(range[i] - mean, 4);
	}
	sigma = sigma / Math.pow(jStat.stdev(range, true), 4);
	return ((n * (n + 1)) / ((n - 1) * (n - 2) * (n - 3))) * sigma - 3
			* (n - 1) * (n - 1) / ((n - 2) * (n - 3));
};

excel.statistical.LARGE = function(range, k) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	k = excel.utils.parseNumber(k);
	if (excel.utils.anyIsError(range, k)) {
		return range;
	}
	return range.sort(function(a, b) {
		return b - a;
	})[k - 1];
};

excel.statistical.LINEST = function(data_y, data_x) {
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	if (excel.utils.anyIsError(data_y, data_x)) {
		return excel.error.value;
	}
	var ymean = jStat.mean(data_y);
	var xmean = jStat.mean(data_x);
	var n = data_x.length;
	var num = 0;
	var den = 0;
	for ( var i = 0; i < n; i++) {
		num += (data_x[i] - xmean) * (data_y[i] - ymean);
		den += Math.pow(data_x[i] - xmean, 2);
	}
	var m = num / den;
	var b = ymean - m * xmean;
	return [ m, b ];
};

// According to Microsoft:
// http://office.microsoft.com/en-us/starter-help/logest-function-HP010342665.aspx
// LOGEST returns are based on the following linear model:
// ln y = x1 ln m1 + ... + xn ln mn + ln b
excel.statistical.LOGEST = function(data_y, data_x) {
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	if (excel.utils.anyIsError(data_y, data_x)) {
		return excel.error.value;
	}
	for ( var i = 0; i < data_y.length; i++) {
		data_y[i] = Math.log(data_y[i]);
	}

	var result = excel.statistical.LINEST(data_y, data_x);
	result[0] = Math.round(Math.exp(result[0]) * 1000000) / 1000000;
	result[1] = Math.round(Math.exp(result[1]) * 1000000) / 1000000;
	return result;
};

excel.statistical.LOGNORM = {};

excel.statistical.LOGNORM.DIST = function(x, mean, sd, cumulative) {
	x = excel.utils.parseNumber(x);
	mean = excel.utils.parseNumber(mean);
	sd = excel.utils.parseNumber(sd);
	if (excel.utils.anyIsError(x, mean, sd)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.lognormal.cdf(x, mean, sd) : jStat.lognormal
			.pdf(x, mean, sd);
};

excel.statistical.LOGNORM.INV = function(probability, mean, sd) {
	probability = excel.utils.parseNumber(probability);
	mean = excel.utils.parseNumber(mean);
	sd = excel.utils.parseNumber(sd);
	if (excel.utils.anyIsError(probability, mean, sd)) {
		return excel.error.value;
	}
	return jStat.lognormal.inv(probability, mean, sd);
};

excel.statistical.MAX = function() {
	var range = excel.utils.numbers(excel.utils.flatten(arguments));
	return (range.length === 0) ? 0 : Math.max.apply(Math, range);
};

excel.statistical.MAXA = function() {
	var range = excel.utils
			.arrayValuesToNumbers(excel.utils.flatten(arguments));
	return (range.length === 0) ? 0 : Math.max.apply(Math, range);
};

excel.statistical.MEDIAN = function() {
	var range = excel.utils
			.arrayValuesToNumbers(excel.utils.flatten(arguments));
	return jStat.median(range);
};

excel.statistical.MIN = function() {
	var range = excel.utils.numbers(excel.utils.flatten(arguments));
	return (range.length === 0) ? 0 : Math.min.apply(Math, range);
};

excel.statistical.MINA = function() {
	var range = excel.utils
			.arrayValuesToNumbers(excel.utils.flatten(arguments));
	return (range.length === 0) ? 0 : Math.min.apply(Math, range);
};

excel.statistical.MODE = {};

excel.statistical.MODE.MULT = function() {
	// Credits: Roönaän
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var n = range.length;
	var count = {};
	var maxItems = [];
	var max = 0;
	var currentItem;

	for ( var i = 0; i < n; i++) {
		currentItem = range[i];
		count[currentItem] = count[currentItem] ? count[currentItem] + 1 : 1;
		if (count[currentItem] > max) {
			max = count[currentItem];
			maxItems = [];
		}
		if (count[currentItem] === max) {
			maxItems[maxItems.length] = currentItem;
		}
	}
	return maxItems;
};

excel.statistical.MODE.SNGL = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	return excel.statistical.MODE.MULT(range).sort(function(a, b) {
		return a - b;
	})[0];
};

excel.statistical.NEGBINOM = {};

excel.statistical.NEGBINOM.DIST = function(k, r, p, cumulative) {
	k = excel.utils.parseNumber(k);
	r = excel.utils.parseNumber(r);
	p = excel.utils.parseNumber(p);
	if (excel.utils.anyIsError(k, r, p)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.negbin.cdf(k, r, p) : jStat.negbin.pdf(k, r, p);
};

excel.statistical.NORM = {};

excel.statistical.NORM.DIST = function(x, mean, sd, cumulative) {
	x = excel.utils.parseNumber(x);
	mean = excel.utils.parseNumber(mean);
	sd = excel.utils.parseNumber(sd);
	if (excel.utils.anyIsError(x, mean, sd)) {
		return excel.error.value;
	}
	if (sd <= 0) {
		return excel.error.num;
	}

	// Return normal distribution computed by jStat [http://jstat.org]
	return (cumulative) ? jStat.normal.cdf(x, mean, sd) : jStat.normal.pdf(x,
			mean, sd);
};

excel.statistical.NORM.INV = function(probability, mean, sd) {
	probability = excel.utils.parseNumber(probability);
	mean = excel.utils.parseNumber(mean);
	sd = excel.utils.parseNumber(sd);
	if (excel.utils.anyIsError(probability, mean, sd)) {
		return excel.error.value;
	}
	return jStat.normal.inv(probability, mean, sd);
};

excel.statistical.NORM.S = {};

excel.statistical.NORM.S.DIST = function(z, cumulative) {
	z = excel.utils.parseNumber(z);
	if (z instanceof Error) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.normal.cdf(z, 0, 1) : jStat.normal.pdf(z, 0, 1);
};

excel.statistical.NORM.S.INV = function(probability) {
	probability = excel.utils.parseNumber(probability);
	if (probability instanceof Error) {
		return excel.error.value;
	}
	return jStat.normal.inv(probability, 0, 1);
};

excel.statistical.PEARSON = function(data_x, data_y) {
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	if (excel.utils.anyIsError(data_y, data_x)) {
		return excel.error.value;
	}
	var xmean = jStat.mean(data_x);
	var ymean = jStat.mean(data_y);
	var n = data_x.length;
	var num = 0;
	var den1 = 0;
	var den2 = 0;
	for ( var i = 0; i < n; i++) {
		num += (data_x[i] - xmean) * (data_y[i] - ymean);
		den1 += Math.pow(data_x[i] - xmean, 2);
		den2 += Math.pow(data_y[i] - ymean, 2);
	}
	return num / Math.sqrt(den1 * den2);
};

excel.statistical.PERCENTILE = {};

excel.statistical.PERCENTILE.EXC = function(array, k) {
	array = excel.utils.parseNumberArray(excel.utils.flatten(array));
	k = excel.utils.parseNumber(k);
	if (excel.utils.anyIsError(array, k)) {
		return excel.error.value;
	}
	array = array.sort(function(a, b) {
		{
			return a - b;
		}
	});
	var n = array.length;
	if (k < 1 / (n + 1) || k > 1 - 1 / (n + 1)) {
		return excel.error.num;
	}
	var l = k * (n + 1) - 1;
	var fl = Math.floor(l);
	return excel.utils.cleanFloat((l === fl) ? array[l] : array[fl] + (l - fl)
			* (array[fl + 1] - array[fl]));
};

excel.statistical.PERCENTILE.INC = function(array, k) {
	array = excel.utils.parseNumberArray(excel.utils.flatten(array));
	k = excel.utils.parseNumber(k);
	if (excel.utils.anyIsError(array, k)) {
		return excel.error.value;
	}
	array = array.sort(function(a, b) {
		return a - b;
	});
	var n = array.length;
	var l = k * (n - 1);
	var fl = Math.floor(l);
	return excel.utils.cleanFloat((l === fl) ? array[l] : array[fl] + (l - fl)
			* (array[fl + 1] - array[fl]));
};

excel.statistical.PERCENTRANK = {};

excel.statistical.PERCENTRANK.EXC = function(array, x, significance) {
	significance = (significance === undefined) ? 3 : significance;
	array = excel.utils.parseNumberArray(excel.utils.flatten(array));
	x = excel.utils.parseNumber(x);
	significance = excel.utils.parseNumber(significance);
	if (excel.utils.anyIsError(array, x, significance)) {
		return excel.error.value;
	}
	array = array.sort(function(a, b) {
		return a - b;
	});
	var uniques = excel.miscellaneous.UNIQUE.apply(null, array);
	var n = array.length;
	var m = uniques.length;
	var power = Math.pow(10, significance);
	var result = 0;
	var match = false;
	var i = 0;
	while (!match && i < m) {
		if (x === uniques[i]) {
			result = (array.indexOf(uniques[i]) + 1) / (n + 1);
			match = true;
		} else if (x >= uniques[i] && (x < uniques[i + 1] || i === m - 1)) {
			result = (array.indexOf(uniques[i]) + 1 + (x - uniques[i])
					/ (uniques[i + 1] - uniques[i]))
					/ (n + 1);
			match = true;
		}
		i++;
	}
	return Math.floor(result * power) / power;
};

excel.statistical.PERCENTRANK.INC = function(array, x, significance) {
	significance = (significance === undefined) ? 3 : significance;
	array = excel.utils.parseNumberArray(excel.utils.flatten(array));
	x = excel.utils.parseNumber(x);
	significance = excel.utils.parseNumber(significance);
	if (excel.utils.anyIsError(array, x, significance)) {
		return excel.error.value;
	}
	array = array.sort(function(a, b) {
		return a - b;
	});
	var uniques = excel.miscellaneous.UNIQUE.apply(null, array);
	var n = array.length;
	var m = uniques.length;
	var power = Math.pow(10, significance);
	var result = 0;
	var match = false;
	var i = 0;
	while (!match && i < m) {
		if (x === uniques[i]) {
			result = array.indexOf(uniques[i]) / (n - 1);
			match = true;
		} else if (x >= uniques[i] && (x < uniques[i + 1] || i === m - 1)) {
			result = (array.indexOf(uniques[i]) + (x - uniques[i])
					/ (uniques[i + 1] - uniques[i]))
					/ (n - 1);
			match = true;
		}
		i++;
	}
	return Math.floor(result * power) / power;
};

excel.statistical.PERMUT = function(number, number_chosen) {
	number = excel.utils.parseNumber(number);
	number_chosen = excel.utils.parseNumber(number_chosen);
	if (excel.utils.anyIsError(number, number_chosen)) {
		return excel.error.value;
	}
	return excel.mathTrig.FACT(number)
			/ excel.mathTrig.FACT(number - number_chosen);
};

excel.statistical.PERMUTATIONA = function(number, number_chosen) {
	number = excel.utils.parseNumber(number);
	number_chosen = excel.utils.parseNumber(number_chosen);
	if (excel.utils.anyIsError(number, number_chosen)) {
		return excel.error.value;
	}
	return Math.pow(number, number_chosen);
};

excel.statistical.PHI = function(x) {
	x = excel.utils.parseNumber(x);
	if (x instanceof Error) {
		return excel.error.value;
	}
	return Math.exp(-0.5 * x * x) / SQRT2PI;
};

excel.statistical.POISSON = {};

excel.statistical.POISSON.DIST = function(x, mean, cumulative) {
	x = excel.utils.parseNumber(x);
	mean = excel.utils.parseNumber(mean);
	if (excel.utils.anyIsError(x, mean)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.poisson.cdf(x, mean) : jStat.poisson.pdf(x,
			mean);
};

excel.statistical.PROB = function(range, probability, lower, upper) {
	if (lower === undefined) {
		return 0;
	}
	upper = (upper === undefined) ? lower : upper;

	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	probability = excel.utils
			.parseNumberArray(excel.utils.flatten(probability));
	lower = excel.utils.parseNumber(lower);
	upper = excel.utils.parseNumber(upper);
	if (excel.utils.anyIsError(range, probability, lower, upper)) {
		return excel.error.value;
	}

	if (lower === upper) {
		return (range.indexOf(lower) >= 0) ? probability[range.indexOf(lower)]
				: 0;
	}

	var sorted = range.sort(function(a, b) {
		return a - b;
	});
	var n = sorted.length;
	var result = 0;
	for ( var i = 0; i < n; i++) {
		if (sorted[i] >= lower && sorted[i] <= upper) {
			result += probability[range.indexOf(sorted[i])];
		}
	}
	return result;
};

excel.statistical.QUARTILE = {};

excel.statistical.QUARTILE.EXC = function(range, quart) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	quart = excel.utils.parseNumber(quart);
	if (excel.utils.anyIsError(range, quart)) {
		return excel.error.value;
	}
	switch (quart) {
	case 1:
		return excel.statistical.PERCENTILE.EXC(range, 0.25);
	case 2:
		return excel.statistical.PERCENTILE.EXC(range, 0.5);
	case 3:
		return excel.statistical.PERCENTILE.EXC(range, 0.75);
	default:
		return excel.error.num;
	}
};

excel.statistical.QUARTILE.INC = function(range, quart) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	quart = excel.utils.parseNumber(quart);
	if (excel.utils.anyIsError(range, quart)) {
		return excel.error.value;
	}
	switch (quart) {
	case 1:
		return excel.statistical.PERCENTILE.INC(range, 0.25);
	case 2:
		return excel.statistical.PERCENTILE.INC(range, 0.5);
	case 3:
		return excel.statistical.PERCENTILE.INC(range, 0.75);
	default:
		return excel.error.num;
	}
};

excel.statistical.RANK = {};

excel.statistical.RANK.AVG = function(number, range, order) {
	number = excel.utils.parseNumber(number);
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	if (excel.utils.anyIsError(number, range)) {
		return excel.error.value;
	}
	range = excel.utils.flatten(range);
	order = order || false;
	var sort = (order) ? function(a, b) {
		return a - b;
	} : function(a, b) {
		return b - a;
	};
	range = range.sort(sort);

	var length = range.length;
	var count = 0;
	for ( var i = 0; i < length; i++) {
		if (range[i] === number) {
			count++;
		}
	}

	return (count > 1) ? (2 * range.indexOf(number) + count + 1) / 2 : range
			.indexOf(number) + 1;
};

excel.statistical.RANK.AVG = function(number, range, order) {
	number = excel.utils.parseNumber(number);
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	if (excel.utils.anyIsError(number, range)) {
		return excel.error.value;
	}
	order = order || false;
	var sort = (order) ? function(a, b) {
		return a - b;
	} : function(a, b) {
		return b - a;
	};
	range = range.sort(sort);
	return range.indexOf(number) + 1;
};
excel.statistical.RANK.EQ = function(number, range, order) {
	number = utils.parseNumber(number);
	range = utils.parseNumberArray(utils.flatten(range));
	if (utils.anyIsError(number, range)) {
		return error.value;
	}
	order = order || false;
	var sort = (order) ? function(a, b) {
		return a - b;
	} : function(a, b) {
		return b - a;
	};
	range = range.sort(sort);
	return range.indexOf(number) + 1;
};

excel.statistical.ROW = function(matrix, index) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	if (index < 0) {
		return excel.error.num;
	}

	if (!(matrix instanceof Array) || (typeof index !== 'number')) {
		return excel.error.value;
	}

	if (matrix.length === 0) {
		return undefined;
	}

	return jStat.row(matrix, index);
};

excel.statistical.ROWS = function(matrix) {
	if (arguments.length !== 1) {
		return excel.error.na;
	}

	if (!(matrix instanceof Array)) {
		return excel.error.value;
	}

	if (matrix.length === 0) {
		return 0;
	}

	return jStat.rows(matrix);
};

excel.statistical.RSQ = function(data_x, data_y) { // no need to flatten here,
	// PEARSON will take care of
	// that
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	if (excel.utils.anyIsError(data_x, data_y)) {
		return excel.error.value;
	}
	return Math.pow(excel.statistical.PEARSON(data_x, data_y), 2);
};

excel.statistical.SKEW = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var mean = jStat.mean(range);
	var n = range.length;
	var sigma = 0;
	for ( var i = 0; i < n; i++) {
		sigma += Math.pow(range[i] - mean, 3);
	}
	return n * sigma
			/ ((n - 1) * (n - 2) * Math.pow(jStat.stdev(range, true), 3));
};

excel.statistical.SKEW.P = function() {
	var range = excel.utils.parseNumberArray(excel.utils.flatten(arguments));
	if (range instanceof Error) {
		return range;
	}
	var mean = jStat.mean(range);
	var n = range.length;
	var m2 = 0;
	var m3 = 0;
	for ( var i = 0; i < n; i++) {
		m3 += Math.pow(range[i] - mean, 3);
		m2 += Math.pow(range[i] - mean, 2);
	}
	m3 = m3 / n;
	m2 = m2 / n;
	return m3 / Math.pow(m2, 3 / 2);
};

excel.statistical.SLOPE = function(data_y, data_x) {
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	if (excel.utils.anyIsError(data_y, data_x)) {
		return excel.error.value;
	}
	var xmean = jStat.mean(data_x);
	var ymean = jStat.mean(data_y);
	var n = data_x.length;
	var num = 0;
	var den = 0;
	for ( var i = 0; i < n; i++) {
		num += (data_x[i] - xmean) * (data_y[i] - ymean);
		den += Math.pow(data_x[i] - xmean, 2);
	}
	return num / den;
};

excel.statistical.SMALL = function(range, k) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	k = excel.utils.parseNumber(k);
	if (excel.utils.anyIsError(range, k)) {
		return range;
	}
	return range.sort(function(a, b) {
		return a - b;
	})[k - 1];
};

excel.statistical.STANDARDIZE = function(x, mean, sd) {
	x = excel.utils.parseNumber(x);
	mean = excel.utils.parseNumber(mean);
	sd = excel.utils.parseNumber(sd);
	if (excel.utils.anyIsError(x, mean, sd)) {
		return excel.error.value;
	}
	return (x - mean) / sd;
};

excel.statistical.STDEV = {};

excel.statistical.STDEV.P = function() {
	var v = excel.statistical.VAR.P.apply(this, arguments);
	return Math.sqrt(v);
};

excel.statistical.STDEV.S = function() {
	var v = excel.statistical.VAR.S.apply(this, arguments);
	return Math.sqrt(v);
};

excel.statistical.STDEVA = function() {
	var v = excel.statistical.VARA.apply(this, arguments);
	return Math.sqrt(v);
};

excel.statistical.STDEVPA = function() {
	var v = excel.statistical.VARPA.apply(this, arguments);
	return Math.sqrt(v);
};

excel.statistical.STEYX = function(data_y, data_x) {
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	if (excel.utils.anyIsError(data_y, data_x)) {
		return excel.error.value;
	}
	var xmean = jStat.mean(data_x);
	var ymean = jStat.mean(data_y);
	var n = data_x.length;
	var lft = 0;
	var num = 0;
	var den = 0;
	for ( var i = 0; i < n; i++) {
		lft += Math.pow(data_y[i] - ymean, 2);
		num += (data_x[i] - xmean) * (data_y[i] - ymean);
		den += Math.pow(data_x[i] - xmean, 2);
	}
	return Math.sqrt((lft - num * num / den) / (n - 2));
};

excel.statistical.TRANSPOSE = function(matrix) {
	if (!matrix) {
		return excel.error.na;
	}
	return jStat.transpose(matrix);
};

excel.statistical.T = excel.text.T;

excel.statistical.T.DIST = function(x, df, cumulative) {
	x = excel.utils.parseNumber(x);
	df = excel.utils.parseNumber(df);
	if (excel.utils.anyIsError(x, df)) {
		return excel.error.value;
	}
	return (cumulative) ? jStat.studentt.cdf(x, df) : jStat.studentt.pdf(x, df);
};

excel.statistical.T.DIST['2T'] = function(x, df) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	if (x < 0 || df < 1) {
		return excel.error.num;
	}

	if ((typeof x !== 'number') || (typeof df !== 'number')) {
		return excel.error.value;
	}

	return (1 - jStat.studentt.cdf(x, df)) * 2;
};

excel.statistical.T.DIST.RT = function(x, df) {
	if (arguments.length !== 2) {
		return excel.error.na;
	}

	if (x < 0 || df < 1) {
		return excel.error.num;
	}

	if ((typeof x !== 'number') || (typeof df !== 'number')) {
		return excel.error.value;
	}

	return 1 - jStat.studentt.cdf(x, df);
};

excel.statistical.T.INV = function(probability, df) {
	probability = excel.utils.parseNumber(probability);
	df = excel.utils.parseNumber(df);
	if (excel.utils.anyIsError(probability, df)) {
		return excel.error.value;
	}
	return jStat.studentt.inv(probability, df);
};

excel.statistical.T.INV['2T'] = function(probability, df) {
	probability = excel.utils.parseNumber(probability);
	df = excel.utils.parseNumber(df);
	if (probability <= 0 || probability > 1 || df < 1) {
		return excel.error.num;
	}
	if (excel.utils.anyIsError(probability, df)) {
		return excel.error.value;
	}
	return Math.abs(jStat.studentt.inv(probability / 2, df));
};

// The algorithm can be found here:
// http://www.chem.uoa.gr/applets/AppletTtest/Appl_Ttest2.html
excel.statistical.T.TEST = function(data_x, data_y) {
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	if (excel.utils.anyIsError(data_x, data_y)) {
		return excel.error.value;
	}

	var mean_x = jStat.mean(data_x);
	var mean_y = jStat.mean(data_y);
	var s_x = 0;
	var s_y = 0;
	var i;

	for (i = 0; i < data_x.length; i++) {
		s_x += Math.pow(data_x[i] - mean_x, 2);
	}
	for (i = 0; i < data_y.length; i++) {
		s_y += Math.pow(data_y[i] - mean_y, 2);
	}

	s_x = s_x / (data_x.length - 1);
	s_y = s_y / (data_y.length - 1);

	var t = Math.abs(mean_x - mean_y)
			/ Math.sqrt(s_x / data_x.length + s_y / data_y.length);

	return excel.statistical.T.DIST['2T'](t, data_x.length + data_y.length - 2);
};

excel.statistical.TREND = function(data_y, data_x, new_data_x) {
	data_y = excel.utils.parseNumberArray(excel.utils.flatten(data_y));
	data_x = excel.utils.parseNumberArray(excel.utils.flatten(data_x));
	new_data_x = excel.utils.parseNumberArray(excel.utils.flatten(new_data_x));
	if (excel.utils.anyIsError(data_y, data_x, new_data_x)) {
		return excel.error.value;
	}
	var linest = excel.statistical.LINEST(data_y, data_x);
	var m = linest[0];
	var b = linest[1];
	var result = [];

	new_data_x.forEach(function(x) {
		result.push(m * x + b);
	});

	return result;
};

excel.statistical.TRIMMEAN = function(range, percent) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	percent = excel.utils.parseNumber(percent);
	if (excel.utils.anyIsError(range, percent)) {
		return excel.error.value;
	}
	var trim = excel.mathTrig.FLOOR(range.length * percent, 2) / 2;
	return jStat.mean(excel.utils.initial(excel.utils.rest(range.sort(function(
			a, b) {
		return a - b;
	}), trim), trim));
};

excel.statistical.VAR = {};

excel.statistical.VAR.P = function() {
	var range = excel.utils.numbers(excel.utils.flatten(arguments));
	var n = range.length;
	var sigma = 0;
	var mean = excel.statistical.AVERAGE(range);
	for ( var i = 0; i < n; i++) {
		sigma += Math.pow(range[i] - mean, 2);
	}
	return sigma / n;
};

excel.statistical.VAR.S = function() {
	var range = excel.utils.numbers(excel.utils.flatten(arguments));
	var n = range.length;
	var sigma = 0;
	var mean = excel.statistical.AVERAGE(range);
	for ( var i = 0; i < n; i++) {
		sigma += Math.pow(range[i] - mean, 2);
	}
	return sigma / (n - 1);
};

excel.statistical.VARA = function() {
	var range = excel.utils.flatten(arguments);
	var n = range.length;
	var sigma = 0;
	var count = 0;
	var mean = excel.statistical.AVERAGEA(range);
	for ( var i = 0; i < n; i++) {
		var el = range[i];
		if (typeof el === 'number') {
			sigma += Math.pow(el - mean, 2);
		} else if (el === true) {
			sigma += Math.pow(1 - mean, 2);
		} else {
			sigma += Math.pow(0 - mean, 2);
		}

		if (el !== null) {
			count++;
		}
	}
	return sigma / (count - 1);
};

excel.statistical.VARPA = function() {
	var range = excel.utils.flatten(arguments);
	var n = range.length;
	var sigma = 0;
	var count = 0;
	var mean = excel.statistical.AVERAGEA(range);
	for ( var i = 0; i < n; i++) {
		var el = range[i];
		if (typeof el === 'number') {
			sigma += Math.pow(el - mean, 2);
		} else if (el === true) {
			sigma += Math.pow(1 - mean, 2);
		} else {
			sigma += Math.pow(0 - mean, 2);
		}

		if (el !== null) {
			count++;
		}
	}
	return sigma / count;
};

excel.statistical.WEIBULL = {};

excel.statistical.WEIBULL.DIST = function(x, alpha, beta, cumulative) {
	x = excel.utils.parseNumber(x);
	alpha = excel.utils.parseNumber(alpha);
	beta = excel.utils.parseNumber(beta);
	if (excel.utils.anyIsError(x, alpha, beta)) {
		return excel.error.value;
	}
	return (cumulative) ? 1 - Math.exp(-Math.pow(x / beta, alpha)) : Math.pow(
			x, alpha - 1)
			* Math.exp(-Math.pow(x / beta, alpha))
			* alpha
			/ Math.pow(beta, alpha);
};

excel.statistical.Z = {};

excel.statistical.Z.TEST = function(range, x, sd) {
	range = excel.utils.parseNumberArray(excel.utils.flatten(range));
	x = excel.utils.parseNumber(x);
	if (excel.utils.anyIsError(range, x)) {
		return excel.error.value;
	}

	sd = sd || excel.statistical.STDEV.S(range);
	var n = range.length;
	return 1 - excel.statistical.NORM.S.DIST(
			(excel.statistical.AVERAGE(range) - x) / (sd / Math.sqrt(n)), true);
};
