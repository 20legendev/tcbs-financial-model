excel.lookup = {};

excel.lookup.MATCH = function(lookupValue, lookupArray, matchType) {
	if (!lookupValue && !lookupArray) {
		return excel.error.na;
	}

	if (arguments.length === 2) {
		matchType = 1;
	}
	if (!(lookupArray instanceof Array)) {
		return excel.error.na;
	}

	if (matchType !== -1 && matchType !== 0 && matchType !== 1) {
		return excel.error.na;
	}
	var index;
	var indexValue;
	for ( var idx = 0; idx < lookupArray.length; idx++) {
		if (matchType === 1) {
			if (lookupArray[idx] === lookupValue) {
				return idx + 1;
			} else if (lookupArray[idx] < lookupValue) {
				if (!indexValue) {
					index = idx + 1;
					indexValue = lookupArray[idx];
				} else if (lookupArray[idx] > indexValue) {
					index = idx + 1;
					indexValue = lookupArray[idx];
				}
			}
		} else if (matchType === 0) {
			if (typeof lookupValue === 'string') {
				lookupValue = lookupValue.replace(/\?/g, '.');
				if (lookupArray[idx].toLowerCase().match(
						lookupValue.toLowerCase())) {
					return idx + 1;
				}
			} else {
				if (lookupArray[idx] === lookupValue) {
					return idx + 1;
				}
			}
		} else if (matchType === -1) {
			if (lookupArray[idx] === lookupValue) {
				return idx + 1;
			} else if (lookupArray[idx] > lookupValue) {
				if (!indexValue) {
					index = idx + 1;
					indexValue = lookupArray[idx];
				} else if (lookupArray[idx] < indexValue) {
					index = idx + 1;
					indexValue = lookupArray[idx];
				}
			}
		}
	}

	return index ? index : excel.error.na;
};
