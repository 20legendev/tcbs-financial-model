excel.logical = {};
excel.logical.AND = function() {
	var args = excel.utils.flatten(arguments);
	var result = true;
	for ( var i = 0; i < args.length; i++) {
		if (!args[i]) {
			result = false;
		}
	}
	return result;
};

excel.logical.CHOOSE = function() {
	if (arguments.length < 2) {
		return excel.error.na;
	}

	var index = arguments[0];
	if (index < 1 || index > 254) {
		return excel.error.value;
	}

	if (arguments.length < index + 1) {
		return excel.error.value;
	}

	return arguments[index];
};

excel.logical.FALSE = function() {
	return false;
};

excel.logical.IF = function(test, then_value, otherwise_value) {
	return test ? then_value : otherwise_value;
};

excel.logical.IFERROR = function(value, valueIfError) {
	if (excel.information.ISERROR(value)) {
		return valueIfError;
	}
	return value;
};

excel.logical.IFNA = function(value, value_if_na) {
	return value === excel.error.na ? value_if_na : value;
};

excel.logical.NOT = function(logical) {
	return !logical;
};

excel.logical.OR = function() {
	var args = excel.utils.flatten(arguments);
	var result = false;
	for ( var i = 0; i < args.length; i++) {
		if (args[i]) {
			result = true;
		}
	}
	return result;
};

excel.logical.TRUE = function() {
	return true;
};

excel.logical.XOR = function() {
	var args = excel.utils.flatten(arguments);
	var result = 0;
	for ( var i = 0; i < args.length; i++) {
		if (args[i]) {
			result++;
		}
	}
	return (Math.floor(Math.abs(result)) & 1) ? true : false;
};

excel.logical.SWITCH = function() {
	var result;
	if (arguments.length > 0) {
		var targetValue = arguments[0];
		var argc = arguments.length - 1;
		var switchCount = Math.floor(argc / 2);
		var switchSatisfied = false;
		var defaultClause = argc % 2 === 0 ? null
				: arguments[arguments.length - 1];

		if (switchCount) {
			for ( var index = 0; index < switchCount; index++) {
				if (targetValue === arguments[index * 2 + 1]) {
					result = arguments[index * 2 + 2];
					switchSatisfied = true;
					break;
				}
			}
		}

		if (!switchSatisfied && defaultClause) {
			result = defaultClause;
		}
	}

	return result;
};
