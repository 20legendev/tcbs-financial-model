function setCompatibility(fn, root) {
	if (root) {
		for ( var i in root) {
			fn[i] = root[i];
		}
	}
	return fn;
}

BETADIST = excel.statistical.BETA.DIST;
BETAINV = excel.statistical.BETA.INV;
BINOMDIST = excel.statistical.BINOM.DIST;
CEILING = ISOCEILING = setCompatibility(excel.mathTrig.CEILING.MATH,
		excel.mathTrig.CEILING);
CEILINGMATH = excel.mathTrig.CEILING.MATH;
CEILINGPRECISE = excel.mathTrig.CEILING.PRECISE;
CHIDIST = excel.statistical.CHISQ.DIST;
CHIDISTRT = excel.statistical.CHISQ.DIST.RT;
CHIINV = excel.statistical.CHISQ.INV;
CHIINVRT = excel.statistical.CHISQ.INV.RT;
CHITEST = excel.statistical.CHISQ.TEST;
CONFIDENCE = setCompatibility(excel.statistical.CONFIDENCE.NORM,
		excel.statistical.CONFIDENCE);
COVAR = excel.statistical.COVARIANCE.P;
COVARIANCEP = excel.statistical.COVARIANCE.P;
COVARIANCES = excel.statistical.COVARIANCE.S;
CRITBINOM = excel.statistical.BINOM.INV;
EXPONDIST = excel.statistical.EXPON.DIST;
ERFCPRECISE = excel.engineering.ERFC.PRECISE;
ERFPRECISE = excel.engineering.ERF.PRECISE;
FDIST = excel.statistical.F.DIST;
FDISTRT = excel.statistical.F.DIST.RT;
FINVRT = excel.statistical.F.INV.RT;
FINV = excel.statistical.F.INV;
FLOOR = setCompatibility(excel.mathTrig.FLOOR.MATH, excel.mathTrig.FLOOR);
FLOORMATH = excel.mathTrig.FLOOR.MATH;
FLOORPRECISE = excel.mathTrig.FLOOR.PRECISE;
FTEST = excel.statistical.F.TEST;
GAMMADIST = excel.statistical.GAMMA.DIST;
GAMMAINV = excel.statistical.GAMMA.INV;
GAMMALNPRECISE = excel.statistical.GAMMALN.PRECISE;
HYPGEOMDIST = excel.statistical.HYPGEOM.DIST;
LOGINV = excel.statistical.LOGNORM.INV;
LOGNORMINV = excel.statistical.LOGNORM.INV;
LOGNORMDIST = excel.statistical.LOGNORM.DIST;
MODE = setCompatibility(excel.statistical.MODE.SNGL, excel.statistical.MODE);
MODEMULT = excel.statistical.MODE.MULT;
MODESNGL = excel.statistical.MODE.SNGL;
NEGBINOMDIST = excel.statistical.NEGBINOM.DIST;
NETWORKDAYSINTL = excel.dateTime.NETWORKDAYS.INTL;
NORMDIST = excel.statistical.NORM.DIST;
NORMINV = excel.statistical.NORM.INV;
NORMSDIST = excel.statistical.NORM.S.DIST;
NORMSINV = excel.statistical.NORM.S.INV;
PERCENTILE = setCompatibility(excel.statistical.PERCENTILE.EXC,
		excel.statistical.PERCENTILE);
PERCENTILEEXC = excel.statistical.PERCENTILE.EXC;
PERCENTILEINC = excel.statistical.PERCENTILE.INC;
PERCENTRANK = setCompatibility(excel.statistical.PERCENTRANK.INC,
		excel.statistical.PERCENTRANK);
PERCENTRANKEXC = excel.statistical.PERCENTRANK.EXC;
PERCENTRANKINC = excel.statistical.PERCENTRANK.INC;
POISSON = setCompatibility(excel.statistical.POISSON.DIST,
		excel.statistical.POISSON);
POISSONDIST = excel.statistical.POISSON.DIST;
QUARTILE = setCompatibility(excel.statistical.QUARTILE.INC,
		excel.statistical.QUARTILE);
QUARTILEEXC = excel.statistical.QUARTILE.EXC;
QUARTILEINC = excel.statistical.QUARTILE.INC;
RANK = setCompatibility(excel.statistical.RANK.EQ, excel.statistical.RANK);
RANKAVG = excel.statistical.RANK.AVG;
RANKEQ = excel.statistical.RANK.EQ;
SKEWP = excel.statistical.SKEW.P;
STDEV = setCompatibility(excel.statistical.STDEV.S, excel.statistical.STDEV);
STDEVP = excel.statistical.STDEV.P;
STDEVS = excel.statistical.STDEV.S;
TDIST = excel.statistical.T.DIST;
TDISTRT = excel.statistical.T.DIST.RT;
TINV = excel.statistical.T.INV;
TTEST = excel.statistical.T.TEST;
VAR = setCompatibility(excel.statistical.VAR.S, excel.statistical.VAR);
VARP = excel.statistical.VAR.P;
VARS = excel.statistical.VAR.S;
WEIBULL = setCompatibility(excel.statistical.WEIBULL.DIST,
		excel.statistical.WEIBULL);
WEIBULLDIST = excel.statistical.WEIBULL.DIST;
WORKDAYINTL = excel.dateTime.WORKDAY.INTL;
ZTEST = excel.statistical.Z.TEST;
