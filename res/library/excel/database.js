excel.database = {};
function compact(array) {
	if (!array) {
		return array;
	}
	var result = [];
	for ( var i = 0; i < array.length; ++i) {
		if (!array[i]) {
			continue;
		}
		result.push(array[i]);
	}
	return result;
}

excel.database.FINDFIELD = function(database, title) {
	var index = null;
	for ( var i = 0; i < database.length; i++) {
		if (database[i][0] === title) {
			index = i;
			break;
		}
	}

	// Return error if the input field title is incorrect
	if (index == null) {
		return excel.error.value;
	}
	return index;
};

function findResultIndex(database, criterias) {
	var matches = {};
	for ( var i = 1; i < database[0].length; ++i) {
		matches[i] = true;
	}
	var maxCriteriaLength = criterias[0].length;
	for (i = 1; i < criterias.length; ++i) {
		if (criterias[i].length > maxCriteriaLength) {
			maxCriteriaLength = criterias[i].length;
		}
	}

	for ( var k = 1; k < database.length; ++k) {
		for ( var l = 1; l < database[k].length; ++l) {
			var currentCriteriaResult = false;
			var hasMatchingCriteria = false;
			for ( var j = 0; j < criterias.length; ++j) {
				var criteria = criterias[j];
				if (criteria.length < maxCriteriaLength) {
					continue;
				}

				var criteriaField = criteria[0];
				if (database[k][0] !== criteriaField) {
					continue;
				}
				hasMatchingCriteria = true;
				for ( var p = 1; p < criteria.length; ++p) {
					currentCriteriaResult = currentCriteriaResult
							|| eval(database[k][l] + criteria[p]);
				}
			}
			if (hasMatchingCriteria) {
				matches[l] = matches[l] && currentCriteriaResult;
			}
		}
	}

	var result = [];
	for ( var n = 0; n < database[0].length; ++n) {
		if (matches[n]) {
			result.push(n - 1);
		}
	}
	return result;
}

// Database functions
excel.database.DAVERAGE = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var sum = 0;
	for ( var i = 0; i < resultIndexes.length; i++) {
		sum += targetFields[resultIndexes[i]];
	}
	return resultIndexes.length === 0 ? excel.error.div0 : sum
			/ resultIndexes.length;
};

excel.database.DCOUNT = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	return excel.statistical.COUNT(targetValues);
};

excel.database.DCOUNTA = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	return excel.statistical.COUNTA(targetValues);
};

excel.database.DGET = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	// Return error if no record meets the criteria
	if (resultIndexes.length === 0) {
		return excel.error.value;
	}
	// Returns the #NUM! error value because more than one record
	// meets the
	// criteria
	if (resultIndexes.length > 1) {
		return excel.error.num;
	}

	return targetFields[resultIndexes[0]];
};

excel.database.DMAX = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var maxValue = targetFields[resultIndexes[0]];
	for ( var i = 1; i < resultIndexes.length; i++) {
		if (maxValue < targetFields[resultIndexes[i]]) {
			maxValue = targetFields[resultIndexes[i]];
		}
	}
	return maxValue;
};

excel.database.DMIN = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var minValue = targetFields[resultIndexes[0]];
	for ( var i = 1; i < resultIndexes.length; i++) {
		if (minValue > targetFields[resultIndexes[i]]) {
			minValue = targetFields[resultIndexes[i]];
		}
	}
	return minValue;
};

excel.database.DPRODUCT = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	targetValues = compact(targetValues);
	var result = 1;
	for (i = 0; i < targetValues.length; i++) {
		result *= targetValues[i];
	}
	return result;
};

excel.database.DSTDEV = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	targetValues = compact(targetValues);
	return excel.statistical.STDEV.S(targetValues);
};

excel.database.DSTDEVP = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	targetValues = compact(targetValues);
	return excel.statistical.STDEV.P(targetValues);
};

excel.database.DSUM = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	return excel.mathTrig.SUM(targetValues);
};

excel.database.DVAR = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	return excel.statistical.VAR.S(targetValues);
};

excel.database.DVARP = function(database, field, criteria) {
	// Return error if field is not a number and not a string
	if (isNaN(field) && (typeof field !== "string")) {
		return excel.error.value;
	}
	var resultIndexes = findResultIndex(database, criteria);
	var targetFields = [];
	if (typeof field === "string") {
		var index = excel.database.FINDFIELD(database, field);
		targetFields = excel.utils.rest(database[index]);
	} else {
		targetFields = excel.utils.rest(database[field]);
	}
	var targetValues = [];
	for ( var i = 0; i < resultIndexes.length; i++) {
		targetValues[i] = targetFields[resultIndexes[i]];
	}
	return excel.statistical.VAR.P(targetValues);
};
