AppConfig = {
	server: {
		service: '/modelserver/'
	},
	template: {
		name: 15
	},
	
	
	namespace: 'vn.tcbs.excel.financialmodel',
	data : [ [ {
		value : 10
	} ], [ {
		value : 0.3
	}, {
		value : 0.4
	} ], [ {
		value : 0
	}, {
		value : 1.2,
		type : 1,
		f : 'SUM(C1:C10)'
	} ] ],
	modelData: {
		row: {
			from: 8,
			to: 150
		},
		col: {
			from: 'C',
			to: 'M'
		}
	},
	rawLine : [
			[ '2010', '2011', '2012', '2013', '2014', '2015', '2016',
					'2017', '2018', '2019', '2020' ], [ {
				type : 1,
				name : 'No. Of Shares at FY end (million)',
				id: 'M01_01'
			}, {
				type : 2,
				name : '% Change',
				id: 'M01_02'
			}, {
				type : 1,
				name : 'Cummulative Adjustment Factor',
				id: 'M01_03'
			}, {
				type : 3,
				name : 'Net Sales',
				id: 'M01_04'
			}, {
				type : 2,
				name : '% Change',
				id: 'M01_05'
			}, {
				type : 4,
				name : 'Depreciation & Amortization',
				id: 'M01_06'
			}, {
				type : 2,
				name : '% Depreciation / Gross Fixed Asset',
				id: 'M01_07'
			}, {
				type : 2,
				name : '% Depreciation / Net Fixed Asset',
				id: 'M01_08'
			}, {
				type : 2,
				name : 'Remaining Life of Assets (year)',
				id: 'M01_09'
			}, {
				type : 2,
				name : 'Net Fixed Asset Turnover',
				id: 'M01_10'
			}, {
				type : 2,
				name : 'Capex/Depreciation',
				id: 'M01_11'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M01_12'
			}, {
				type : 4,
				name : 'Other COGS',
				id: 'M01_13'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M01_14'
			}, {
				type : 2,
				name : '% of total COGS',
				id: 'M01_15'
			}, {
				type : 3,
				name : 'COGS',
				id: 'M01_16'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M01_17'
			}, {
				type : 3,
				name : 'Gross Profit',
				id: 'M01_18'
			}, {
				type : 2,
				name : 'Gross Profit Margin %',
				id: 'M01_19'
			}, {
				type : 2,
				name : 'Gross Margin Growth%',
				id: 'M01_20'
			}, {
				type : 2,
				name : 'Gross Profit Growth',
				id: 'M01_21'
			}, {
				type : 4,
				name : 'Operating Exp. - Selling',
				id: 'M01_22'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M01_23'
			}, {
				type : 4,
				name : 'Operating Exp. - G&A',
				id: 'M01_24'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M01_25'
			}, {
				type : 4,
				name : 'Other Operating Exp.',
				id: 'M01_26'
			}, {
				type : 3,
				name : 'Operating Exp.',
				id: 'M01_27'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M01_28'
			}, {
				type : 3,
				name : 'Operating Income',
				id: 'M01_29'
			}, {
				type : 2,
				name : 'Operating Margin %',
				id: 'M01_30'
			}, {
				type : 2,
				name : 'Operating Profit Growth',
				id: 'M01_31'
			}, {
				type : 4,
				name : 'Interest Income',
				id: 'M01_32'
			}, {
				type : 2,
				name : '% of Cash',
				id: 'M01_33'
			}, {
				type : 4,
				name : 'Interest Expenses',
				id: 'M01_34'
			}, {
				type : 2,
				name : '% of Total Interest Bearing Debt',
				id: 'M01_35'
			}, {
				type : 4,
				name : 'Investment Income/Loss',
				id: 'M01_36'
			}, {
				type : 2,
				name : '% of ST & LT Investment',
				id: 'M01_37'
			}, {
				type : 2,
				name : 'Growth',
				id: 'M01_38'
			}, {
				type : 4,
				name : 'Gain/Loss on Disposal Investment.',
				id: 'M01_39'
			}, {
				type : 2,
				name : '% of ST & LT Investment',
				id: 'M01_40'
			}, {
				type : 2,
				name : '% Net Non-Operating Expenses',
				id: 'M01_41'
			}, {
				type : 4,
				name : 'Exchange Gain/Loss',
				id: 'M01_42'
			}, {
				type : 4,
				name : 'Other Non-Operate Inc.',
				id: 'M01_43'
			}, {
				type : 3,
				name : 'Net Non-Op.Income/Expenses',
				id: 'M01_44'
			}, {
				type : 3,
				name : '',
				id: 'M01_45'
			},{
				type : 4,
				name : 'Pre-Tax Income',
				id: 'M01_46'
			}, {
				type : 2,
				name : 'Pre tax Margin%',
				id: 'M01_47'
			}, {
				type : 4,
				name : 'Income Tax Expense',
				id: 'M01_48'
			}, {
				type : 2,
				name : 'Effective Tax Rate %',
				id: 'M01_49'
			}, {
				type : 2,
				name : 'Tax Rate %',
				id: 'M01_50'
			}, {
				type : 3,
				name : 'Income after tax, before Minorities, Extra',
				id: 'M01_51'
			}, {
				type : 4,
				name : 'Minority Interest',
				id: 'M01_52'
			}, {
				type : 2,
				name : '% of Profit after Extraordinary',
				id: 'M01_53'
			}, {
				type : 2,
				name : 'Income b4 Extra Item but after MI',
				id: 'M01_54'
			}, {
				type : 2,
				name : 'Net Profit Margin %',
				id: 'M01_55'
			}, {
				type : 2,
				name : 'Earning Growth %',
				id: 'M01_56'
			}, {
				type : 2,
				name : 'EPS (VND)',
				id: 'M01_57'
			}, {
				type : 2,
				name : 'PE',
				id: 'M01_58'
			}, {
				type : 2,
				name : 'ROE',
				id: 'M01_59'
			}, {
				type : 2,
				name : 'ROA',
				id: 'M01_60'
			}, {
				type : 2,
				name : 'ROCE',
				id: 'M01_61'
			}, {
				type : 4,
				name : 'Total Extraordinary Items',
				id: 'M01_62'
			}, {
				type : 3,
				name : 'Consolidated Net Inc after XO & MI',
				id: 'M01_63'
			}, {
				type : 2,
				name : 'Earning Growth %',
				id: 'M01_64'
			}, {
				type : 4,
				name : 'Dividend payment by cash',
				id: 'M01_65'
			}, {
				type : 2,
				name : 'Dividend Paidout',
				id: 'M01_66'
			}, {
				type : 4,
				name : 'DPS (VND)',
				id: 'M01_67'
			}, {
				type : 5,
				name : 'BALANCE SHEET',
				id: 'M02_00'
			},{
				type : 5,
				name : '',
				id: 'M02_01'
			}, {
				type : 3,
				name : 'ASSETS',
				id: 'M02_02'
			}, {
				type : 4,
				name : 'Cash,Cash Equivalent',
				id: 'M02_03'
			}, {
				type : 4,
				name : 'Marketable Security',
				id: 'M02_04'
			}, {
				type : 3,
				name : 'Total Cash',
				id: 'M02_05'
			}, {
				type : 2,
				name : 'Cash/Current Asset %',
				id: 'M02_06'
			}, {
				type : 2,
				name : '% Growth',
				id: 'M02_07'
			}, {
				type : 3,
				name : 'ST Investments',
				id: 'M02_08'
			}, {
				type : 2,
				name : 'ST Investments/Current Assets %',
				id: 'M02_09'
			}, {
				type : 2,
				name : '% Growth',
				id: 'M02_10'
			}, {
				type : 3,
				name : 'Account Receivables',
				id: 'M02_11'
			}, {
				type : 2,
				name : 'Debtors Days',
				id: 'M02_12'
			}, {
				type : 2,
				name : 'AR/Current Asset %',
				id: 'M02_13'
			}, {
				type : 2,
				name : '% Growth',
				id: 'M02_14'
			}, {
				type : 3,
				name : 'Inventories',
				id: 'M02_15'
			}, {
				type : 2,
				name : 'Inventory Days',
				id: 'M02_16'
			}, {
				type : 2,
				name : 'Inventory/Current Asset %',
				id: 'M02_17'
			}, {
				type : 2,
				name : '% Growth',
				id: 'M02_18'
			}, {
				type : 3,
				name : 'Other Current Assets',
				id: 'M02_19'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M02_20'
			}, {
				type : 3,
				name : 'Total Current Assets',
				id: 'M02_21'
			}, {
				type : 2,
				name : 'Current Assets/Current Liabilities',
				id: 'M02_22'
			}, {
				type : 3,
				name : 'Long-term Investment',
				id: 'M02_23'
			}, {
				type : 2,
				name : 'Growth %',
				id: 'M02_24'
			}, {
				type : 2,
				name : '% Total Asset',
				id: 'M02_25'
			}, {
				type : 4,
				name : 'CAPEX',
				id: 'M02_26'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M02_27'
			}, {
				type : 2,
				name : '% Depreciation',
				id: 'M02_28'
			}, {
				type : 2,
				name : '% of Net Fixed Assets',
				id: 'M02_29'
			}, {
				type : 4,
				name : 'Gross Fixed Assets',
				id: 'M02_30'
			}, {
				type : 3,
				name : 'Net Fixed Assets',
				id: 'M02_31'
			}, {
				type : 2,
				name : '% Total Asset',
				id: 'M02_32'
			}, {
				type : 2,
				name : 'Gross Fixed Asset Turnover',
				id: 'M02_33'
			}, {
				type : 2,
				name : 'Net Fixed Asset Turnover',
				id: 'M02_34'
			}, {
				type: 4,
				name: 'Deferred Charges',
				id: 'M02_35'
			},{
				type : 3,
				name : 'Other Non-Curr.Asset',
				id: 'M02_36'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M02_37'
			}, {
				type : 3,
				name : 'Total Assets',
				id: 'M02_38'
			}, {
				type : 2,
				name : 'Total Assets Turnover',
				id: 'M02_39'
			}, {
				type : 3,
				name : 'Total Short Term Debt',
				id: 'M02_40'
			}, {
				type : 2,
				name : '% Total Interest Bearing Debt',
				id: 'M02_41'
			}, {
				type : 2,
				name : '% Sales',
				id: 'M02_42'
			}, {
				type : 3,
				name : 'Account Payables',
				id: 'M02_43'
			}, {
				type : 2,
				name : 'Payable Days',
				id: 'M02_44'
			}, {
				type : 2,
				name : 'Payables/Current Liabilities %',
				id: 'M02_45'
			}, {
				type : 2,
				name : '% Growth',
				id: 'M02_46'
			}, {
				type : 3,
				name : 'Other Current Liab.',
				id: 'M02_47'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M02_48'
			}, {
				type : 3,
				name : 'Total Current Liab.',
				id: 'M02_49'
			}, {
				type : 3,
				name : '',
				id: 'M02_50'
			}, {
				type : 3,
				name : 'Long Term Debt',
				id: 'M02_51'
			}, {
				type : 2,
				name : '% Interest Bearing Debt',
				id: 'M02_52'
			}, {
				type : 2,
				name : 'Total Interest Bearing Debt',
				id: 'M02_53'
			}, {
				type : 2,
				name : 'Total Debt/Equity',
				id: 'M02_54'
			}, {
				type : 2,
				name : 'Cash',
				id: 'M02_55'
			}, {
				type : 3,
				name : 'Other Long term Liabiliites',
				id: 'M02_56'
			}, {
				type : 2,
				name : '% of Sales',
				id: 'M02_57'
			}, {
				type : 3,
				name : 'Total Liabilities',
				id: 'M02_58'
			}, {
				type : 2,
				name : 'Tt Liabilities/Equity',
				id: 'M02_59'
			}, {
				type : 3,
				name : 'Minority Equity',
				id: 'M02_60'
			}, {
				type : 2,
				name : 'Minority Interest/Equity',
				id: 'M02_61'
			}, {
				type : 4,
				name : 'Paid-in Cap/Chartered Capital',
				id: 'M02_62'
			}, {
				type : 2,
				name : 'New Shares Issued (million)',
				id: 'M02_63'
			}, {
				type : 2,
				name : 'Par Value (VND/share)',
				id: 'M02_64'
			}, {
				type : 2,
				name : 'Issue Price (VND/share)',
				id: 'M02_65'
			}, {
				type : 2,
				name : 'Excess Over Par',
				id: 'M02_66'
			}, {
				type : 2,
				name : 'New bonus issued (million)',
				id: 'M02_67'
			}, {
				type : 2,
				name : '% of existing shares',
				id: 'M02_68'
			}, {
				type : 4,
				name : 'Capital Surplus',
				id: 'M02_69'
			}, {
				type : 4,
				name : 'Retained Earning & Reserves',
				id: 'M02_70'
			}, {
				type : 4,
				name : 'Treasury Stock',
				id: 'M02_71'
			}, {
				type : 2,
				name : 'Number of treasury shares (million)',
				id: 'M02_72'
			}, {
				type : 4,
				name : "Other Shareholders' equity",
				id: 'M02_73'
			}, {
				type : 3,
				name : 'Total Equity',
				id: 'M02_74'
			}

			] ]
};