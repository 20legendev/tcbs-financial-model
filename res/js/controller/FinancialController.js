App.controller('FinancialController', ['$scope','reportService', function($scope, reportService) {

	this.initialize = function() {
		this.config = {};
		this.scope = $scope;
		this.scope.model = {
			rawLine: AppConfig.rawLine
		};
		this.scope.cellSetting = {
			
		};
		
		this.scope.rawLine = AppConfig.rawLine;
		this.loadCommand();
		this.initColorPicker();
		this.initData();
		this.initRange();
		
		this.scope.data = AppConfig.data;
		
		var _self = this;
		this.scope.modelData = AppConfig.modelData;
		this.scope.input = AppConfig.input;
		
		/*
		var _self = this;
		this.scope.inputChange = function(col, row){
			_self.scope.data[0][0].value = _self.scope.data[2][1].value + _self.scope.data[1][1].value/2;
			_self.scope.data[1][0].value = Math.sqrt(_self.scope.data[2][1].value);
		};
		*/
		
		this.scope.editCommand = function(varName){
			_self.editCommand(varName);
		};
		this.scope.commandEdited = function(){
			_self.commandEdited();
		};
		this.scope.inputChange = function(){
			_self.refresh();
		};
		this.scope.saveTemplate = function(){
			_self.saveTemplate();
		};
		this.scope.excelValue = window;
	};
	
	this.initColorPicker = function(){
		var _self = this;
		this.scope.rgbPicker = { 'background-color': '' };
		this.scope.$on('colorpicker-selected', function(event, data){
			_self.scope.rgbPicker['background-color'] = data.value;
			var cell = _self.scope.cellSetting[_self.scope.model.curEditCell];
			if(!cell) cell = {};
			if(!cell.style) cell.style = {};
			cell.style['background-color'] = data.value;
			_self.saveCellSetting();
		});
	},
	
	this.refresh = function(){
		try{
			for(var col in this.scope.cellSetting){
				if(this.scope.cellSetting[col].command){
					this.scope.excelValue[col] = eval(ExcelHelper.formatCommand(this.scope.cellSetting[col].command));
				}
			}
		}catch(ex){
			console.log("ERROR " , ex);
		}
		this.scope.$apply();
	},
	
	this.editCommand = function(varName){
		if(!this.scope.cellSetting[varName]) this.scope.cellSetting[varName] = {};
		var cell = this.scope.cellSetting[varName];
		this.scope.curCommand = cell.command ? cell.command : "";
		this.scope.model.curEditCell = varName;
		if(!cell.style) cell.style = {};
		this.scope.rgbPicker['background-color'] = cell.style['background-color'] ? cell.style['background-color'] : '#FFF';
	},
	
	this.commandEdited = function(){
		var curCell = this.scope.cellSetting[this.scope.model.curEditCell]; 
		curCell.command = this.scope.curCommand;
		try{
			this.scope.excelValue[this.scope.model.curEditCell] = eval(ExcelHelper.formatCommand(this.scope.curCommand));
			this.scope.$apply();
		}catch(ex){
			console.log(ex);
		};
		this.scope.rgbPicker['background-color'] = curCell.style['background-color'] ? curCell.style['background-color'] : '#FFF';
		 
		this.saveCellSetting();
	},
	
	this.initRange = function(){
		this.scope.model.rangeCol = [];
		this.scope.model.rangeRow = [];
		for(var i = AppConfig.modelData.row.from; i < AppConfig.modelData.row.to; i++){
			this.scope.model.rangeCol.push(i);
		}
		
		var from = AppConfig.modelData.col.from.charCodeAt(0);
		var to = AppConfig.modelData.col.to.charCodeAt(0);
		for(var j = from; j <= to; j++){
			this.scope.model.rangeRow.push(String.fromCharCode(j));
		}
	};
	
	this.loadCommand = function(){
		var _self = this;
		reportService.loadTemplate(function(data){
			_self.scope.cellSetting = JSON.parse(data.data.templateData);
			_self.refresh();
		});
	},
	
	this.initData = function(){
		var years = this.scope.model.rawLine[0];
		this.scope.yearData = {};
		var tmp;
		for(var i=0; i < years.length; i++){
			tmp = reportService.getByYear(years[i]);
			if(tmp != undefined){
				this.scope.yearData[years[i]] = tmp;
			}
		}
		//TODO : optimize code
		this.bindServerData();
		this.bindTemplateCommand();
	};
	
	this.bindTemplateCommand = function(){
		var years = this.scope.model.rawLine[0],
			colname = AppConfig.modelData.col.from,
			colpos = 0,
			curYear = years[colpos],
			rowData = this.scope.model.rawLine[1],
			data = this.scope.yearData[curYear];
		
		for(var j=0; j < years.length; j++){
			for(var i=0; i < rowData.length; i++){
				if(data){
					tmp = colname + (i + AppConfig.modelData.row.from);
					if(this.scope.cellSetting[tmp] && this.scope.cellSetting[tmp].command){
						try{
							window[tmp] = eval(ExcelHelper.formatCommand(this.scope.cellSetting[tmp].command));
						}catch(ex){
							console.log(ex);
						}
					}
				}
			};
			colpos++;
			curYear = years[colpos];
			colname = String.fromCharCode(colname.charCodeAt(0) + 1);
			data = this.scope.yearData[curYear];
		};
	},
	
	this.bindServerData = function(){
		
		var from = AppConfig.modelData.col.from.charCodeAt(0),
			to = AppConfig.modelData.col.to.charCodeAt(0),
			years = this.scope.model.rawLine[0],
			
			colname = AppConfig.modelData.col.from,
			colpos = 0,
			curYear = years[colpos],
			rowData = this.scope.model.rawLine[1],
			data = this.scope.yearData[curYear];
		
		var tmp;
		for(var j=0; j < years.length; j++){
			for(var i=0; i < rowData.length; i++){
				tmp = colname + (i + AppConfig.modelData.row.from);
				if(data && data[rowData[i].id] != undefined){
					window[tmp] = data[rowData[i].id];
				}else{
					window[tmp] = undefined;
				}
			};
			colpos++;
			curYear = years[colpos];
			colname = String.fromCharCode(colname.charCodeAt(0) + 1);
			data = this.scope.yearData[curYear];
		};
	};
	
	this.saveCellSetting = function(){
		localStorage.setItem(AppConfig.namespace + '.cell.setting', JSON.stringify(this.scope.cellSetting));
	};
	
	this.saveTemplate = function(){
		var data = JSON.stringify(this.scope.cellSetting);
		reportService.saveTemplate(data, function(res){
			console.log(res);
		});
	};
	
	
	
	this.initialize();
}]);