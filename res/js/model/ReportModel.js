App.factory('ReportModel', ['$http',
function($http) {
	function ReportModel(year) {
		this.config = {
			year : year
		};
	}


	ReportModel.prototype = {
		getTemplateData : function(cb) {
			$http.post(AppConfig.server.service + 'api/template/load', {
				templateDataId : AppConfig.template.name
			}).success(function(data, status, header, config) {
				if (cb)
					cb(data);
			});
		},

		saveTemplate : function(data, cb) {
			$http.post(AppConfig.server.service + 'api/template/save', {
				templateDataId : AppConfig.template.name,
				templateData : data
			}).success(function(data, status, header, config) {
				if (cb)
					cb(data);
			});
		}
	};

	return ReportModel;
}]);
