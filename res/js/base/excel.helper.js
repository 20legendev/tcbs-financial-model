ExcelHelper = {
	formatCommand: function(command){
		var origin = command;
		if(command.indexOf('=') == 0){
			command = command.substr(1);
		}
		var found = command.match(/[a-zA-Z0-9]+:[a-zA-Z0-9]+/g);
		var tmp, tmpStr = "";
		if(found && found.length > 0){
			for(var i=0; i < found.length; i++){
				tmp = found[i].split(':');
				if(tmp.length > 0){
					var col = tmp[0].slice(0,1);
					var from = tmp[0].slice(1);
					var to = tmp[1].slice(1);
					command = command.replace(found[i], "ExcelHelper.range('"+col+"', "+from+", "+to+")");
				}
			}
		}
		return command;
	},
	
	range: function(col, from, to){
		var arr = [];
		for(var i = from; i <= to; i++){
			if(window[col + i] != undefined){
				arr.push(window[col + i]);
			}
		}
		return arr;
	}
};
