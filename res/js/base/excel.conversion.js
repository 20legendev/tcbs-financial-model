ASC = function() {
	return excel.text.ASC.apply(null, arguments);
};

BAHTTEXT = function() {
	return excel.text.BAHTTEXT.apply(null, arguments);
};
CHAR = function(number) {
	return excel.text.CHAR(number);
};
CLEAN = function(text) {
	return excel.text.CLEAN(text);
};
CODE = function(text) {
	return excel.text.CODE(text);
};
CONCATENATE = function() {
	return excel.text.CONCATENATE.apply(null, arguments);
};
DBCS = function() {
	return excel.text.DBCS.apply(null, arguments);
};
DOLLAR = function(number, decimals) {
	return excel.text.DOLLAR(number, decimals);
};
EXACT = function(text1, text2) {
	return excel.text.EXACT(text1, text2);
};
FIND = function(find_text, within_text, position) {
	return excel.text.FIND(find_text, within_text, position);
};
FIXED = function(number, decimals, no_commas) {
	return excel.text.FIXED(number, decimals, no_commas);
};
HTML2TEXT = function(value) {
	return excel.text.HTML2TEXT(value);
};
LEFT = function(text, number) {
	return excel.text.LEFT(text, number);
};
LEN = function(text) {
	return excel.text.LEN(text);
};
LOWER = function(text) {
	return excel.text.LOWER(text);
};
MID = function(text, start, number) {
	return excel.text.MID(text, start, number);
};
NUMBERVALUE = function(text, decimal_separator, group_separator) {
	return excel.text.NUMBERVALUE(text, decimal_separator, group_separator);
};
PRONETIC = function() {
	return excel.text.PRONETIC();
};
PROPER = function(text) {
	return excel.text.PROPER(text);
};
REGEXEXTRACT = function(text, regular_expression) {
	return excel.text.REGEXEXTRACT(text, regular_expression);
};
REGEXMATCH = function(text, regular_expression, full) {
	return excel.text.REGEXMATCH(text, regular_expression, full);
};
REGEXREPLACE = function(text, regular_expression, replacement) {
	return excel.text.REGEXREPLACE(text, regular_expression, replacement);
};
REPLACE = function(text, position, length, new_text) {
	return excel.text.REPLACE(text, position, length, new_text);
};
REPT = function(text, number) {
	return excel.text.REPT(text, number);
};
RIGHT = function(text, number) {
	return excel.text.RIGHT(text, number);
};
SEARCH = function(find_text, within_text, position) {
	return excel.text.SEARCH(find_text, within_text, position);
};
SPLIT = function(text, separator) {
	return excel.text.SPLIT(text, separator);
};
SUBSTITUTE = function(text, old_text, new_text, occurrence) {
	return excel.text.SUBSTITUTE(text, old_text, new_text, occurrence);
};
T = function(value) {
	return excel.text.T(value);
};
TEXT = function(value, format) {
	return excel.text.TEXT(value, format);
};
TRIM = function(text) {
	return excel.text.TRIM(text);
};
UNICHAR = excel.text.UNICHAR;
UNICODE = excel.text.UNICODE;

UPPER = function(text) {
	return excel.text.UPPER(text);
};
VALUE = function(text) {
	return excel.text.VALUE(text);
};

// Statistical
AVEDEV = function() {
	return excel.statistical.AVEDEV.apply(null, arguments);
};
AVERAGE = function() {
	return excel.statistical.AVERAGE.apply(null, arguments);
};
AVERAGEA = function() {
	return excel.statistical.AVERAGEA.apply(null, arguments);
};
AVERAGEIF = function(range, criteria, average_range) {
	return excel.statistical.AVERAGEIF(range, criteria, average_range);
};
AVERAGEIFS = function() {
	return excel.statistical.AVERAGEIFS.apply(null, arguments);
};
BETA = excel.statistical.BETA;

BETA.DIST = function(x, alpha, beta, cumulative, A, B) {
	return excel.statistical.BETA.DIST(x, alpha, beta, cumulative, A, B);
};
BETA.INV = function(probability, alpha, beta, A, B) {
	return excel.statistical.BETA.INV(probability, alpha, beta, A, B);
};
BETADIST = function(x, alpha, beta, cumulative, A, B) {
	return excel.statistical.BETA.DIST(x, alpha, beta, cumulative, A, B);
};
BETAINV = function(probability, alpha, beta, A, B) {
	return excel.statistical.BETA.INV(probability, alpha, beta, A, B);
};
BINOM = excel.statistical.BINOM;

BINOM.DIST = function(successes, trials, probability, cumulative) {
	return excel.statistical.BINOM.DIST(successes, trials, probability,
			cumulative);
};
BINOM.DIST.RANGE = function(trials, probability, successes, successes2) {
	return excel.statistical.BINOM.DIST.RANGE(trials, probability, successes,
			successes2);
};
BINOM.INV = function(trials, probability, alpha) {
	return excel.statistical.BINOM.INV(trials, probability, alpha);
};
BINOMDIST = function(successes, trials, probability, cumulative) {
	return excel.statistical.BINOM.DIST(successes, trials, probability,
			cumulative);
};
CHISQ = excel.statistical.CHISQ;

CHISQ.DIST = function(x, k, cumulative) {
	return excel.statistical.CHISQ.DIST(x, k, cumulative);
};
CHISQ.DIST.RT = function(x, k) {
	return excel.statistical.CHISQ.DIST.RT(x, k);
};
CHISQ.INV = function(probability, k) {
	return excel.statistical.CHISQ.INV(probability, k);
};
CHISQ.INV.RT = function(p, k) {
	return excel.statistical.CHISQ.INV.RT(p, k);
};
CHISQ.TEST = function(observed, expected) {
	return excel.statistical.CHISQ.TEST(observed, expected);
};
COLUMN = function(matrix, index) {
	return excel.statistical.COLUMN(matrix, index);
};
COLUMNX = function(matrix) {
	return excel.statistical.COLUMNS(matrix);
};
CONFIDENCE = excel.statistical.CONFIDENCE;
CONFIDENCE.NORM = function(alpha, sd, n) {
	return excel.statistical.CONFIDENCE.NORM(alpha, sd, n);
};
CONFIDENCE.T = function(alpha, sd, n) {
	return excel.statistical.CONFIDENCE.T(alpha, sd, n);
};
CORREL = function(array1, array2) {
	return excel.statistical.CORREL(array1, array2);
};
COUNT = function() {
	return excel.statistical.COUNT.apply(null, arguments);
};
COUNTA = function() {
	return excel.statistical.COUNTA.apply(null, arguments);
};
COUNTIN = function(range, value) {
	return excel.statistical.COUNTIN(range, value);
};
COUNTBLANK = function() {
	return excel.statistical.COUNTBLANK.apply(null, arguments);
};
COUNTIF = function(range, criteria) {
	return excel.statistical.COUNTIF(range, criteria);
};
COUNTIFS = function() {
	return excel.statistical.COUNTIFS.apply(null, arguments);
};
COUNTUNIQUE = function() {
	return excel.statistical.COUNTUNIQUE.apply(null, arguments);
};
COVARIANCE = excel.statistical.COVARIANCE;
COVARIANCE.P = function(array1, array2) {
	return excel.statistical.COVARIANCE.P(array1, array2);
};

COVARIANCE.S = function(array1, array2) {
	return excel.statistical.COVARIANCE.S(array1, array2);
};
DEVSQ = function() {
	return excel.statistical.DEVSQ.apply(null, arguments);
};
EXPON = excel.statistical.EXPON;

EXPON.DIST = function(x, lambda, cumulative) {
	return excel.statistical.EXPON.DIST(x, lambda, cumulative);
};
F = excel.statistical.F;
F.DIST = function(x, d1, d2, cumulative) {
	return excel.statistical.F.DIST(x, d1, d2, cumulative);
};
F.DIST.RT = function(x, d1, d2) {
	return excel.statistical.F.DIST.RT(x, d1, d2);
};
F.INV = function(probability, d1, d2) {
	return excel.statistical.F.INV(probability, d1, d2);
};
F.INV.RT = function(p, d1, d2) {
	return excel.statistical.F.INV.RT(p, d1, d2);
};
F.TEST = function(array1, array2) {
	return excel.statistical.F.TEST(array1, array2);
};
FISHER = function(x) {
	return excel.statistical.FISHER(x);
};
FISHERINV = function(y) {
	return excel.statistical.FISHERINV(y);
};
FORECAST = function(x, data_y, data_x) {
	return excel.statistical.FORECAST(x, data_y, data_x);
};
FREQUENCY = function(data, bins) {
	return excel.statistical.FREQUENCY(data, bins);
};
GAMMA = function(number) {
	return excel.statistical.GAMMA(number);
};
GAMMA.DIST = function(value, alpha, beta, cumulative) {
	return excel.statistical.GAMMA.DIST(value, alpha, beta, cumulative);
};
GAMMA.INV = function(probability, alpha, beta) {
	return excel.statistical.GAMMA.INV(probability, alpha, beta);
};
GAMMALN = function(number) {
	return excel.statistical.GAMMALN(number);
};
GAMMALN.PRECISE = function(x) {
	return excel.statistical.GAMMALN.PRECISE(x);
};
GAUSS = function(z) {
	return excel.statistical.GAUSS(z);
};
GEOMEAN = function() {
	return excel.statistical.GEOMEAN.apply(null, arguments);
};
GROWTH = function(known_y, known_x, new_x, use_const) {
	return excel.statistical.GROWTH(known_y, known_x, new_x, use_const);
};
HARMEAN = function() {
	return excel.statistical.HARMEAN.apply(null, arguments);
};
HYPGEOM = excel.statistical.HYPGEOM;
HYPGEOM.DIST = function(x, n, M, N, cumulative) {
	return excel.statistical.HYPGEOM.DIST(x, n, M, N, cumulative);
};
INTERCEPT = function(known_y, known_x) {
	return excel.statistical.INTERCEPT(known_y, known_x);
};
KURT = function() {
	return excel.statistical.KURT.apply(null, arguments);
};
LARGE = function(range, k) {
	return excel.statistical.LARGE(range, k);
};
LINEST = function(data_y, data_x) {
	return excel.statistical.LINEST(data_y, data_x);
};
LOGEST = function(data_y, data_x) {
	return excel.statistical.LOGEST(data_y, data_x);
};
LOGNORM = excel.statistical.LOGNORM;
LOGNORM.DIST = function(x, mean, sd, cumulative) {
	return excel.statistical.LOGNORM.DIST(x, mean, sd, cumulative);
};
LOGNORM.INV = function(probability, mean, sd) {
	return excel.statistical.LOGNORM.INV(probability, mean, sd);
};
MAX = function() {
	return excel.statistical.MAX.apply(null, arguments);
};
MAXA = function() {
	return excel.statistical.MAXA.apply(null, arguments);
};
MEDIAN = function() {
	return excel.statistical.MEDIAN.apply(null, arguments);
};
MIN = function() {
	return excel.statistical.MIN.apply(null, arguments);
};
MINA = function() {
	return excel.statistical.MINA.apply(null, arguments);
};
MODE = excel.statistical.MODE;
MODE.MULT = function() {
	return excel.statistical.MODE.MULT.apply(null, arguments);
};
MODE.SNGL = function() {
	return excel.statistical.MODE.SNGL.apply(null, arguments);
};
NEGBINOM = excel.statistical.NEGBINOM;
NEGBINOM.DIST = function(k, r, p, cumulative) {
	return excel.statistical.NEGBINOM.DIST(k, r, p, cumulative);
};
NORM = excel.statistical.NORM;
NORM.DIST = function(x, mean, sd, cumulative) {
	return excel.statistical.NORM.DIST(x, mean, sd, cumulative);
};
NORM.INV = function(probability, mean, sd) {
	return excel.statistical.NORM.INV(probability, mean, sd);
};
NORM.S = excel.statistical.NORM.S;
NORM.S.DIST = function(z, cumulative) {
	return excel.statistical.NORM.S.DIST(z, cumulative);
};
NORM.S.INV = function(probability) {
	return excel.statistical.NORM.S.INV(probability);
};
PEARSON = function(data_x, data_y) {
	return excel.statistical.PEARSON(data_x, data_y);
};
PERCENTILE = excel.statistical.PERCENTILE;
PERCENTILE.EXC = function(array, k) {
	return excel.statistical.PERCENTILE.EXC(array, k);
};
PERCENTILE.INC = function(array, k) {
	return excel.statistical.PERCENTILE.INC(array, k);
};
PERCENTRANK = excel.statistical.PERCENTRANK;
PERCENTRANK.EXC = function(array, x, significance) {
	return excel.statistical.PERCENTRANK.EXC(array, x, significance);
};
PERCENTRANK.INC = function(array, x, significance) {
	return excel.statistical.PERCENTRANK.INC(array, x, significance);
};
PERMUT = function(number, number_chosen) {
	return excel.statistical.PERMUT(number, number_chosen);
};
PERMUTATIONA = function(number, number_chosen) {
	return excel.statistical.PERMUTATIONA(number, number_chosen);
};
PHI = function(x) {
	return excel.statistical.PHI(x);
};
POISSON = excel.statistical.POISSON;
POISSON.DIST = function(x, mean, cumulative) {
	return excel.statistical.POISSON.DIST(x, mean, cumulative);
};
PROB = function(range, probability, lower, upper) {
	return excel.statistical.PROB(range, probability, lower, upper);
};
QUARTILE = excel.statistical.QUARTILE;
QUARTILE.EXC = function(range, quart) {
	return excel.statistical.QUARTILE.EXC(range, quart);
};
QUARTILE.INC = function(range, quart) {
	return excel.statistical.QUARTILE.INC(range, quart);
};
RANK = excel.statistical.RANK;
RANK.AVG = function(number, range, order) {
	return excel.statistical.RANK.AVG(number, range, order);
};
RANK.EQ = function(number, range, order) {
	return excel.statistical.RANK.EQ(number, range, order);
};
ROW = function(matrix, index) {
	return excel.statistical.ROW(matrix, index);
};
ROWS = function(matrix) {
	return excel.statistical.ROWS(matrix);
};
RSQ = function(data_x, data_y) {
	return excel.statistical.RSQ(data_x, data_y);
};
SKEW = function() {
	return excel.statistical.SKEY.apply(null, arguments);
};
SKEW.P = function() {
	return excel.statistical.SKEW.P.apply(null, arguments);
};
SLOPE = function(data_y, data_x) {
	return excel.statistical.SLOPE(data_y, data_x);
};
SMALL = function(range, k) {
	return excel.statistical.SMALL(range, k);
};
STANDARDIZE = function(x, mean, sd) {
	return excel.statistical.STANDARDIZE(x, mean, sd);
};
STDEV = excel.statistical.STDEV;

STDEV.P = function() {
	return excel.statistical.STDEV.P.apply(null, arguments);
};
STDEV.S = function() {
	return excel.statistical.STDEV.S.apply(null, arguments);
};
STDEVA = function() {
	return excel.statistical.STDEVA.apply(null, arguments);
};
STDEVPA = function() {
	return excel.statistical.STDEVPA.apply(null, arguments);
};
STEYX = function(data_y, data_x) {
	return excel.statistical.STEYX(data_y, data_x);
};
TRANSPOSE = function(matrix) {
	return excel.statistical.TRANSPOSE(matrix);
};
T = excel.text.T;
T.DIST = function(x, df, cumulative) {
	return excel.statistical.T.DIST(x, df, cumulative);
};
T.DIST['2T'] = function(x, df) {
	return excel.statistical.T.DIST['2T'](x, df);
};
T.DIST.RT = function(x, df) {
	return excel.statistical.T.DIST.RT(x, df);
};
T.INV = function(probability, df) {
	return excel.statistical.T.INV(probability, df);
};
T.INV['2T'] = function(probability, df) {
	return excel.statistical.T.INV['2T'](probability, df);
};
T.TEST = function(data_x, data_y) {
	return excel.statistical.T.TEST(data_x, data_y);
};
TREND = function(data_y, data_x, new_data_x) {
	return excel.statistical.TREND(data_y, data_x, new_data_x);
};
TRIMMEAN = function(range, percent) {
	return excel.statistical.TRIMMEAN(range, percent);
};

VAR = excel.statistical.VAR;
VAR.P = function() {
	return excel.statistical.VAR.P.apply(null, arguments);
};
VAR.S = function() {
	return excel.statistical.VAR.S.apply(null, arguments);
};

VARA = function() {
	return excel.statistical.VARA.apply(null, arguments);
};
VARPA = function() {
	return excel.statistical.VARPA.apply(null, arguments);
};

WEIBULL = excel.statistical.WEIBULL;
WEIBULL.DIST = function(x, alpha, beta, cumulative) {
	return excel.statistical.WEIBULL.DIST(x, alpha, beta, cumulative);
};

Z = excel.statistical.Z;
Z.TEST = function(range, x, sd) {
	return excel.statistical.Z.TEST(range, x, sd);
};

// Miscellaneous
UNIQUE = function() {
	return excel.miscellaneous.UNIQUE.apply(null, arguments);
};

ARGS2ARRAY = function() {
	return excel.miscellaneous.ARGS2ARRAY.apply(null, arguments);
};
REFERENCE = function(context, reference) {
	return excel.miscellaneous.REFERENCE(context, reference);
};
JOIN = function(array, separator) {
	return excel.miscellaneous.JOIN(array, separator);
};
NUMBERS = function() {
	return excel.miscellaneous.NUMBERS.apply(null, arguments);
};
NUMERAL = function(number, format) {
	return excel.miscellaneous.NUMERAL(number, format);
};

// MathTrig
ABS = function(number) {
	return excel.mathTrig.ABS(number);
};
ACOS = function(number) {
	return excel.mathTrig.ACOS(number);
};
ACOSH = function(number) {
	return excel.mathTrig.ACOSH(number);
};
ACOT = function(number) {
	return excel.mathTrig.ACOT(number);
};
ACOTH = function(number) {
	return excel.mathTrig.ACOTH(number);
};
AGGREGATE = function(function_num, options, ref1, ref2) {
	return excel.mathTrig.AGGREGATE(function_num, options, ref1, ref2);
};
ARABIC = function(text) {
	return excel.mathTrig.ARABIC(text);
};
ASIN = function(number) {
	return excel.mathTrig.ASIN(number);
};
ASINH = function(number) {
	return excel.mathTrig.ASINH(number);
};
ATAN = function(number) {
	return excel.mathTrig.ATAN(number);
};

ATAN2 = function(number_x, number_y) {
	return excel.mathTrig.ATAN2(number_x, number_y);
};
ATANH = function(number) {
	return excel.mathTrig.ATANH(number);
};
BASE = function(number, radix, min_length) {
	return excel.mathTrig.BASE(number, radix, min_length);
};
CEILING = function(number, significance, mode) {
	return excel.mathTrig.CEILING(number, significance, mode);
};
CEILING.MATH = excel.mathTrig.CEILING;
CEILING.PRECISE = excel.mathTrig.CEILING;

COMBIN = function(number, number_chosen) {
	return excel.mathTrig.COMBIN(number, number_chosen);
};
COMBINA = function(number, number_chosen) {
	return excel.mathTrig.COMBINA(number, number_chosen);
};
COS = function(number) {
	return excel.mathTrig.COS(number);
};
COSH = function(number) {
	return excel.mathTrig.COSH(number);
};
COT = function(number) {
	return excel.mathTrig.COT(number);
};
COTH = function(number) {
	return excel.mathTrig.COTH(number);
};
CSC = function(number) {
	return excel.mathTrig.CSC(number);
};
CSCH = function(number) {
	return excel.mathTrig.CSCH(number);
};
DECIMAL = function(number, radix) {
	return excel.mathTrig.DECIMAL(number, radix);
};
DEGREES = function(number) {
	return excel.mathTrig.DEGREES(number);
};
EVEN = function(number) {
	return excel.mathTrig.EVEN(number);
};
EXP = Math.exp;

FACT = function(number) {
	return excel.mathTrig.FACT(number);
};
FACTDOUBLE = function(number) {
	return excel.mathTrig.FACTDOUBLE(number);
};
FLOOR = function(number, significance) {
	return excel.mathTrig.FLOOR(number, significance);
};
FLOOR.MATH = function(number, significance, mode) {
	return excel.mathTrig.FLOOR.MATH(number, significance, mode);
};
FLOOR.PRECISE = excel.mathTrig.FLOOR.MATH;

GCD = function() {
	return excel.mathTrig.GCD.apply(null, arguments);
};
INT = function(number) {
	return excel.mathTrig.INT(number);
};
ISO = excel.mathTrig.ISO;
LCM = function() {
	return excel.mathTrig.LCM.apply(null, arguments);
};
LN = function(number) {
	return excel.mathTrig.LN(number);
};
LOG = function(number, base) {
	return excel.mathTrig.LOG(number, base);
};
LOG10 = function(number) {
	return excel.mathTrig.LOG10(number);
};
MDETERM = function(matrix) {
	return excel.mathTrig.MDETERM(matrix);
};
MINVERSE = function(matrix) {
	return excel.mathTrig.MINVERSE(matrix);
};
MMULT = function(matrix1, matrix2) {
	return excel.mathTrig.MMULT(matrix1, matrix2);
};
MOD = function(dividend, divisor) {
	return excel.mathTrig.MOD(dividend, divisor);
};
MROUND = function(number, multiple) {
	return excel.mathTrig.MROUND(number, multiple);
};
MULTINOMIAL = function() {
	return excel.mathTrig.MULTINOMIAL.apply(null, arguments);
};
MUNIT = function(dimension) {
	return excel.mathTrig.MUNIT(dimension);
};
ODD = function(number) {
	return excel.mathTrig.ODD(number);
};
PI = function() {
	return excel.mathTrig.PI.apply(null, arguments);
};
POWER = function(number, power) {
	return excel.mathTrig.POWER(number, power);
};
PRODUCT = function() {
	return excel.mathTrig.PRODUCT.apply(null, arguments);
};
QUOTIENT = function(numerator, denominator) {
	return excel.mathTrig.QUOTIENT(numerator, denominator);
};
RADIANS = function(number) {
	return excel.mathTrig.RADIANS(number);
};
RAND = function() {
	return excel.mathTrig.RAND.apply(null, arguments);
};
RANDBETWEEN = function(bottom, top) {
	return excel.mathTrig.RANDBETWEEN(bottom, top);
};
ROMAN = function(number) {
	return excel.mathTrig.ROMAN(number);
};
ROUND = function(number, digits) {
	return excel.mathTrig.ROUND(number, digits);
};
ROUNDDOWN = function(number, digits) {
	return excel.mathTrig.ROUNDDOWN(number, digits);
};
ROUNDUP = function(number, digits) {
	return excel.mathTrig.ROUNDUP(number, digits);
};
SEC = function(number) {
	return excel.mathTrig.SEC(number);
};
SECH = function(number) {
	return excel.mathTrig.SECH(number);
};
SERIESSUM = function(x, n, m, coefficients) {
	return excel.mathTrig.SERIESSUM(x, n, m, coefficients);
};
SIGN = function(number) {
	return excel.mathTrig.SIGN(number);
};
SIN = function(number) {
	return excel.mathTrig.SIN(number);
};
SINH = function(number) {
	return excel.mathTrig.SINH(number);
};
SQRT = function(number) {
	return excel.mathTrig.SQRT(number);
};
SQRTPI = function(number) {
	return excel.mathTrig.SQRTPI(number);
};
SUBTOTAL = function(function_code, ref1) {
	return excel.mathTrig.SUBTOTAL(function_code, ref1);
};
ADD = function(num1, num2) {
	return excel.mathTrig.ADD(num1, num2);
};
MINUS = function(num1, num2) {
	return excel.mathTrig.MINUS(num1, num2);
};
DIVIDE = function(dividend, divisor) {
	return excel.mathTrig.DIVIDE(dividend, divisor);
};
MULTIPLY = function(factor1, factor2) {
	return excel.mathTrig.MULTIPLY(factor1, factor2);
};
GTE = function(num1, num2) {
	return excel.mathTrig.GTE(num1, num2);
};
LT = function(num1, num2) {
	return excel.mathTrig.LT(num1, num2);
};
LTE = function(num1, num2) {
	return excel.mathTrig.LTE(num1, num2);
};
EQ = function(value1, value2) {
	return excel.mathTrig.EQ(value1, value2);
};
NE = function(value1, value2) {
	return excel.mathTrig.NE(value1, value2);
};
POW = function(base, exponent) {
	return excel.mathTrig.POW(base, exponent);
};
SUM = function() {
	return excel.mathTrig.SUM.apply(null, arguments);
};
SUMIF = function(range, criteria) {
	return excel.mathTrig.SUMIF(range, criteria);
};
SUMIFS = function() {
	return excel.mathTrig.SUMIFS.apply(null, arguments);
};
SUMPRODUCT = function() {
	return excel.mathTrig.SUMPRODUCT.apply(null, arguments);
};
SUMSQ = function() {
	return excel.mathTrig.SUMSQ.apply(null, arguments);
};
SUMX2MY2 = function(array_x, array_y) {
	return excel.mathTrig.SUMX2MY2(array_x, array_y);
};
SUMX2PY2 = function(array_x, array_y) {
	return excel.mathTrig.SUMX2PY2(array_x, array_y);
};
SUMXMY2 = function(array_x, array_y) {
	return excel.mathTrig.SUMXMY2(array_x, array_y);
};
TAN = function(number) {
	return excel.mathTrig.TAN(number);
};
TANH = function(number) {
	return excel.mathTrig.TANH(number);
};
TRUNC = function(number, digits) {
	return excel.mathTrig.TRUNC(number, digits);
};

// Lookup reference
MATCH = function(lookupValue, lookupArray, matchType) {
	return excel.lookup.MATCH(lookupValue, lookupArray, matchType);
};

// Logical
AND = function() {
	return excel.logical.AND.apply(null, arguments);
};
CHOOSE = function() {
	return excel.logical.CHOOSE.apply(null, arguments);
};
FALSE = function() {
	return excel.logical.FALSE.apply(null, arguments);
};
IF = function(test, then_value, otherwise_value) {
	return excel.logical.IF(test, then_value, otherwise_value);
};
IFERROR = function(value, valueIfError) {
	return excel.logical.IFERROR(value, valueIfError);
};
IFNA = function(value, value_if_na) {
	return excel.logical.IFNA(value, value_if_na);
};
NOT = function(logical) {
	return excel.logical.NOT(logical);
};
OR = function() {
	return excel.logical.OR.apply(null, arguments);
};
OR = function() {
	return excel.logical.TRUE.apply(null, arguments);
};
TRUE = function() {
	return excel.logical.XOR.apply(null, arguments);
};
XOR = function() {
	return excel.logical.SWITCH.apply(null, arguments);
};
SWITCH = function() {
	return excel.logical.SWITCH.apply(null, arguments);
};

// Information
CELL = function() {
	return excel.information.CELL.apply(null, arguments);
};
ERROR = excel.information.ERROR;
ERROR.TYPE = function(error_val) {
	return excel.information.ERROR.TYPE(error_val);
};
INFO = function() {
	return excel.information.INFO.apply(null, arguments);
};
ISBLANK = function(value) {
	return excel.information.ISBLANK(value);
};
ISBINARY = function(number) {
	return excel.information.ISBINARY(number);
};
ISERR = function(value) {
	return excel.information.ISERR(value);
};
ISERROR = function(value) {
	return excel.information.ISERROR(value);
};
ISEVEN = function(number) {
	return excel.information.ISEVEN(number);
};
ISFORMULA = function() {
	return excel.information.ISFORMULA.apply(null, arguments);
};
ISLOGICAL = function(value) {
	return excel.information.ISLOGICAL(value);
};
ISNA = function(value) {
	return excel.information.ISNA(value);
};
ISNONTEXT = function(value) {
	return excel.information.ISNONTEXT(value);
};
ISNUMBER = function(value) {
	return excel.information.ISNUMBER(value);
};
ISODD = function(number) {
	return excel.information.ISODD(number);
};
ISREF = function() {
	return excel.information.ISREF.apply(null, arguments);
};
ISTEXT = function(value) {
	return excel.information.ISTEXT(value);
};
N = function(value) {
	return excel.information.N(value);
};
NA = function() {
	return excel.information.NA.apply(null, arguments);
};
SHEET = function() {
	return excel.information.SHEET.apply(null, arguments);
};
SHEETS = function() {
	return excel.information.SHEETS.apply(null, arguments);
};
TYPE = function(value) {
	return excel.information.TYPE(value);
};

// Financial
ACCRINT = function(issue, first, settlement, rate, par,frequency, basis){
	return excel.financial.ACCRINT(issue, first, settlement, rate, par, frequency, basis);
};
ACCRINTM = function() {
	return excel.financial.ACCRINTM.apply(null, arguments);
};
AMORDEGRC = function() {
	return excel.financial.AMORDEGRC.apply(null, arguments);
};
AMORLINC = function() {
	return excel.financial.AMORLINC.apply(null, arguments);
};
COUPDAYBS = function() {
	return excel.financial.COUPDAYBS.apply(null, arguments);
};
COUPDAYS = function() {
	return excel.financial.COUPDAYS.apply(null, arguments);
};
COUPDAYSNC = function() {
	return excel.financial.COUPDAYSNC.apply(null, arguments);
};
COUPNCD = function() {
	return excel.financial.COUPNCD.apply(null, arguments);
};
COUPNUM = function() {
	return excel.financial.COUPNUM.apply(null, arguments);
};
COUPPCD = function() {
	return excel.financial.COUPPCD.apply(null, arguments);
};
CUMIPMT = function(rate, periods, value, start, end, type) {
	return excel.financial.CUMIPMT(rate, periods, value, start, end, type);
};
CUMPRINC = function(rate, periods, value, start, end, type) {
	return excel.financial.CUMPRINC(rate, periods, value, start, end, type);
};
DB = function(cost, salvage, life, period, month) {
	return excel.financial.DB(cost, salvage, life, period, month);
};
DDB = function(cost, salvage, life, period, factor) {
	return excel.financial.DDB(cost, salvage, life, period, factor);
};
DISC = function() {
	return excel.financial.DISC.apply(null, arguments);
};
DOLLARDE = function(dollar, fraction) {
	return excel.financial.DOLLARDE(dollar, fraction);
};
DOLLARFR = function(dollar, fraction) {
	return excel.financial.DOLLARFR(dollar, fraction);
};		
DURATION = function() {
	return excel.financial.DURATION.apply(null, arguments);
};
EFFECT = function(rate, periods) {
	return excel.financial.EFFECT(rate, periods);
};
FV = function(rate, periods, payment, value, type) {
	return excel.financial.FV(rate, periods, payment, value, type);
};
FVSCHEDULE = function(principal, schedule) {
	return excel.financial.FVSCHEDULE(principal, schedule);
};
INTRATE = function() {
	return excel.financial.INTRATE.apply(null, arguments);
};
IPMT = function(rate, period, periods, present, future, type) {
	return excel.financial.IPMT(rate, period, periods, present, future, type);
};
IRR = function(values, guess) {
	return excel.financial.IRR(values, guess);
};
ISPMT = function(rate, period, periods, value) {
	return excel.financial.ISPMT(rate, period, periods, value);
};
MDURATION = function() {
	return excel.financial.MDURATION.apply(null, arguments);
};
MIRR = function(values, finance_rate, reinvest_rate) {
	return excel.financial.MIRR(values, finance_rate, reinvest_rate);
};
NOMINAL = function(rate, periods) {
	return excel.financial.NOMINAL(rate, periods);
};
NPER = function(rate, payment, present, future, type) {
	return excel.financial.NPER(rate, payment, present, future, type);
};
NPV = function() {
	return excel.financial.NPV.apply(null, arguments);
};
ODDFPRICE = function() {
	return excel.financial.ODDFPRICE.apply(null, arguments);
};
ODDFYIELD = function() {
	return excel.financial.ODDFYIELD.apply(null, arguments);
};
ODDLPRICE = function() {
	return excel.financial.ODDLPRICE.apply(null, arguments);
};
ODDLYIELD = function() {
	return excel.financial.ODDLYIELD.apply(null, arguments);
};
PDURATION = function(rate, present, future) {
	return excel.financial.PDURATION(rate, present, future);
};
PMT = function(rate, periods, present, future, type) {
	return excel.financial.PMT(rate, periods, present, future, type);
};
PPMT = function(rate, period, periods, present, future, type) {
	return excel.financial.PPMT(rate, period, periods, present, future, type);
};
PRICE = function() {
	return excel.financial.PRICE.apply(null, arguments);
};
PRICEDISC = function() {
	return excel.financial.PRICEDISC.apply(null, arguments);
};
PRICEMAT = function() {
	return excel.financial.PRICEMAT.apply(null, arguments);
};
PV = function(rate, periods, payment, future, type) {
	return excel.financial.PV(rate, periods, payment, future, type);
};
RATE = function(periods, payment, present, future, type, guess) {
	return excel.financial.RATE(periods, payment, present, future, type, guess);
};
RECEIVED = function() {
	return excel.financial.RECEIVED.apply(null, arguments);
};
RRI = function(periods, present, future) {
	return excel.financial.RRI(periods, present, future);
};
SLN = function(cost, salvage, life) {
	return excel.financial.SLN(cost, salvage, life);
};
SYD = function(cost, salvage, life, period) {
	return excel.financial.SYD(cost, salvage, life, period);
};
TBILLEQ = function(settlement, maturity, discount) {
	return excel.financial.TBILLEQ(settlement, maturity, discount);
};
TBILLPRICE = function(settlement, maturity, discount) {
	return excel.financial.TBILLPRICE(settlement, maturity, discount);
};
TBILLYIELD = function(settlement, maturity, price) {
	return excel.financial.TBILLYIELD(settlement, maturity, price);
};
VDB = function() {
	return excel.financial.VDB.apply(null, arguments);
};
XIRR = function(values, dates, guess) {
	return excel.financial.XIRR(values, dates, guess);
};
XNPV = function(rate, values, dates) {
	return excel.financial.XNPV(rate, values, dates);
};
YIELD = function() {
	return excel.financial.YIELD.apply(null, arguments);
};
YIELDDISC = function() {
	return excel.financial.YIELDDISC.apply(null, arguments);
};
YIELDMAT = function() {
	return excel.financial.YIELDMAT.apply(null, arguments);
};

// Engineering

BESSELI = function(x, n) {
	return excel.engineering.BESSELI(x, n);
};
BESSELJ = function(x, n) {
	return excel.engineering.BESSELJ(x, n);
};
BESSELK = function(x, n) {
	return excel.engineering.BESSELK(x, n);
};
BESSELY = function(x, n) {
	return excel.engineering.BESSELY(x, n);
};
BIN2DEC = function(number) {
	return excel.engineering.BIN2DEC(number);
};
BIN2HEX = function(number, places) {
	return excel.engineering.BIN2HEX(number, places);
};
BIN2OCT = function(number, places) {
	return excel.engineering.BIN2OCT(number, places);
};
BITAND = function(number1, number2) {
	return excel.engineering.BITAND(number1, number2);
};
BITLSHIFT = function(number, shift) {
	return excel.engineering.BITLSHIFT(number, shift);
};
BITOR = function(number1, number2) {
	return excel.engineering.BITOR(number1, number2);
};
BITRSHIFT = function(number, shift) {
	return excel.engineering.BITRSHIFT(number, shift);
};
BITXOR = function(number1, number2) {
	return excel.engineering.BITXOR(number1, number2);
};
COMPLEX = function(real, imaginary, suffix) {
	return excel.engineering.COMPLEX(real, imaginary, suffix);
};
CONVERT = function(number, from_unit, to_unit) {
	return excel.engineering.CONVERT(number, from_unit, to_unit);
};
DEC2BIN = function(number, places) {
	return excel.engineering.DEC2BIN(number, places);
};
DEC2HEX = function(number, places) {
	return excel.engineering.DEC2HEX(number, places);
};
DEC2OCT = function(number, places) {
	return excel.engineering.DEC2OCT(number, places);
};
DELTA = function(number1, number2) {
	return excel.engineering.DELTA(number1, number2);
};
ERF = function(lower_bound, upper_bound) {
	return excel.engineering.ERF(lower_bound, upper_bound);
};
ERF.PRECISE = function() {
	return excel.engineering.ERF.PRECISE.apply(null, arguments);
};
ERFC = function(x) {
	return excel.engineering.ERFC(x);
};
ERFC.PRECISE = function() {
	return excel.engineering.ERFC.PRECISE.apply(null, arguments);
};
GESTEP = function(number, step) {
	return excel.engineering.GESTEP(number, step);
};
HEX2BIN = function(number, places) {
	return excel.engineering.HEX2BIN(number, places);
};
HEX2DEC = function(number) {
	return excel.engineering.HEX2DEC(number);
};
HEX2OCT = function(number, places) {
	return excel.engineering.HEX2OCT(number, places);
};
IMABS = function(inumber) {
	return excel.engineering.IMABS(inumber);
};
IMAGINARY = function(inumber) {
	return excel.engineering.IMAGINARY(inumber);
};
IMARGUMENT = function(inumber) {
	return excel.engineering.IMARGUMENT(inumber);
};
IMCONJUGATE = function(inumber) {
	return excel.engineering.IMCONJUGATE(inumber);
};
IMCOS = function(inumber) {
	return excel.engineering.IMCOS(inumber);
};
IMCOSH = function(inumber) {
	return excel.engineering.IMCOSH(inumber);
};
IMCOT = function(inumber) {
	return excel.engineering.IMCOT(inumber);
};
IMDIV = function(inumber1, inumber2){
	return excel.engineering.IMDIV(inumber1, inumber2);
};
IMEXP = function(inumber) {
	return excel.engineering.IMEXP(inumber);
};
IMLN = function(inumber) {
	return excel.engineering.IMLN(inumber);
};
IMLOG10 = function(inumber) {
	return excel.engineering.IMLOG10(inumber);
};
IMLOG2 = function(inumber) {
	return excel.engineering.IMLOG2(inumber);
};
IMPOWER = function(inumber, number) {
	return excel.engineering.IMPOWER(inumber, number);
};
IMPRODUCT = function() {
	return excel.engineering.IMPRODUCT.apply(null, arguments);
};
IMREAL = function(inumber) {
	return excel.engineering.IMREAL(inumber);
};
IMSEC = function(inumber) {
	return excel.engineering.IMSEC(inumber);
};
IMSECH = function(inumber) {
	return excel.engineering.IMSECH(inumber);
};
IMSIN = function(inumber) {
	return excel.engineering.IMSIN(inumber);
};
IMSINH = function(inumber) {
	return excel.engineering.IMSINH(inumber);
};
IMSQRT = function(inumber) {
	return excel.engineering.IMSQRT(inumber);
};
IMCSC = function(inumber) {
	return excel.engineering.IMCSC(inumber);
};
IMCSCH = function(inumber) {
	return excel.engineering.IMCSCH(inumber);
};
IMSUB = function(inumber1, inumber2) {
	return excel.engineering.IMSUB(inumber1, inumber2);
};
IMSUM = function() {
	return excel.engineering.IMSUM.apply(null, arguments);
};
IMTAN = function(inumber) {
	return excel.engineering.IMTAN(inumber);
};
OCT2BIN = function(number, places) {
	return excel.engineering.OCT2BIN(number, places);
};
OCT2DEC = function(number) {
	return excel.engineering.OCT2DEC(number);
};
OCT2HEX = function(number, places) {
	return excel.engineering.OCT2HEX(number, places);
};

// Datetime
DATE = function(year, month, day) {
	return excel.dateTime.DATE(year, month, day);
};
DATEVALUE = function(date_text) {
	return excel.dateTime.DATEVALUE(date_text);
};
DAY = function(serial_number) {
	return excel.dateTime.DAY(serial_number);
};
DAYS = function(end_date, start_date) {
	return excel.dateTime.DAYS(end_date, start_date);
};
DAYS360 = function(start_date, end_date, method) {
	return excel.dateTime.DAYS360(start_date, end_date, method);
};
EDATE = function(start_date, months) {
	return excel.dateTime.EDATE(start_date, months);
};
EOMONTH = function(start_date, months) {
	return excel.dateTime.EOMONTH(start_date, months);
};
HOUR = function(serial_number) {
	return excel.dateTime.HOUR(serial_number);
};
INTERVAL = function(second) {
	return excel.dateTime.INTERVAL(second);
};
ISOWEEKNUM = function(date) {
	return excel.dateTime.ISOWEEKNUM(date);
};
MINUTE = function(serial_number) {
	return excel.dateTime.MINUTE(serial_number);
};
MONTH = function(serial_number) {
	return excel.dateTime.MONTH(serial_number);
};
NETWORKDAYS = function(start_date, end_date, holidays) {
	return excel.dateTime.NETWORKDAYS(start_date, end_date, holidays);
};
NETWORKDAYS.INTL = function(start_date, end_date, weekend, holidays){
	return excel.dateTime.NETWORKDAYS.INTL(start_date, end_date, weekend, holidays);
};

NOW = function() {
	return excel.dateTime.NOW.apply(null, arguments);
};
SECOND = function(serial_number) {
	return excel.dateTime.SECOND(serial_number);
};
TIME = function(hour, minute, second) {
	return excel.dateTime.TIME(hour, minute, second);
};

TIMEVALUE = function(time_text) {
	return excel.dateTime.TIMEVALUE(time_text);
};
TODAY = function() {
	return excel.dateTime.TODAY.apply(null, arguments);
};
WEEKDAY = function(serial_number, return_type) {
	return excel.dateTime.WEEKDAY(serial_number, return_type);
};
WEEKNUM = function(serial_number, return_type) {
	return excel.dateTime.WEEKNUM(serial_number, return_type);
};
WORKDAY = function(start_date, days, holidays) {
	return excel.dateTime.WORKDAY(start_date, days, holidays);
};
WORKDAY.INTL = function(start_date, days, weekend, holidays) {
	return excel.dateTime.WORKDAY.INTL(start_date, days, weekend, holidays);
};
YEAR = function(serial_number) {
	return excel.dateTime.YEAR(serial_number);
};
YEARFRAC = function(start_date, end_date, basis) {
	return excel.dateTime.YEARFRAC(start_date, end_date, basis);
};

// Database
FINDFIELD = function(database, title) {
	return excel.database.FINDFIELD(database, title);
};
DAVERAGE = function(database, field, criteria) {
	return excel.database.DAVERAGE(database, field, criteria);
};
DCOUNT = function(database, field, criteria) {
	return excel.database.DCOUNT(database, field, criteria);
};
DCOUNTA = function(database, field, criteria) {
	return excel.database.DCOUNTA(database, field, criteria);
};
DGET = function(database, field, criteria) {
	return excel.database.DGET(database, field, criteria);
};
DGET = function(database, field, criteria) {
	return excel.database.DGET(database, field, criteria);
};
DMIN = function(database, field, criteria) {
	return excel.database.DMIN(database, field, criteria);
};
DPRODUCT = function(database, field, criteria) {
	return excel.database.DPRODUCT(database, field, criteria);
};
DSTDEV = function(database, field, criteria) {
	return excel.database.DSTDEV(database, field, criteria);
};
DSTDEVP = function(database, field, criteria) {
	return excel.database.DSTDEVP(database, field, criteria);
};
DSUM = function(database, field, criteria) {
	return excel.database.DSUM(database, field, criteria);
};
DVAR = function(database, field, criteria) {
	return excel.database.DVAR(database, field, criteria);
};
DVARP = function(database, field, criteria) {
	return excel.database.DVARP(database, field, criteria);
};