FormatUtils = {
	format : function(f, number) {
		switch (f.type) {
		case '%':
			if (f.round != undefined) {
				number = Math.round(number * 100 * Math.pow(10, f.round)) / Math.pow(10, f.round);
			}
			return number + '%';
			break;
		default:
			return number;
		}
	}
}; 